﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using zaht02_BP.Helpers;
using zaht02_BP.Models.CodeTables;

namespace zaht02_BP.Controllers
{
    [Route("api/[controller]")]
    [ApiController, Authorize]
    public class CodeTablesController : ControllerConfig
    {
        public CodeTablesController(IOptions<Configuration> configAccesor, ILogger<CodeTablesController> logger) : base(configAccesor, logger)
        {
        }
        // GET: api/CodeTables/5
        [HttpGet("{name}")]
        public IActionResult Get(string name)
        {
            string idName = name + "Id";
            bool isEmployee = name == "Employee";
            List<CodeTableModel> responseList = new List<CodeTableModel>();
            try
            {
                using (SqlConnection connection = new SqlConnection(_config.ConnectionString))
                {
                    connection.Open();
                    string sql = String.Format("SELECT {0}, {1} FROM [dbo].{2}", idName,
                        isEmployee ? "FirstName, LastName" : "Name", name);
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        SqlDataReader dataReader = cmd.ExecuteReader();

                        while (dataReader.Read())
                        {
                            responseList.Add
                                (
                                    new CodeTableModel()
                                    {
                                        Id = (int)dataReader[idName],
                                        Name = isEmployee ? String.Format("{0} {1}",dataReader["FirstName"].ToString(),dataReader["LastName"].ToString())
                                        : dataReader["Name"].ToString()
                                    }
                                );
                        }
                    }
                }
                return Ok(responseList);
            }
            catch (Exception e)
            {
                _logger.LogError(String.Format("{0} {1}", e.Message, e.StackTrace));
                return BadRequest();
            }
        }

        // GET: api/CodeTables/5
        [HttpGet("[action]")]
        public IActionResult GetProjectManagers()
        {
            try
            {
                List<CodeTableModel> responseList = new List<CodeTableModel>();
                using (SqlConnection connection = new SqlConnection(_config.ConnectionString))
                {
                    connection.Open();
                    string sql = "SELECT DISTINCT LastName, FirstName, Employee.EmployeeId FROM [dbo].Employee " +
                        "INNER JOIN [dbo].Role ON Role.EmployeeId = Employee.EmployeeId " +
                        "WHERE [dbo].Role.Role = 'ProjectManager'; ";
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        SqlDataReader dataReader = cmd.ExecuteReader();

                        while (dataReader.Read())
                        {
                            responseList.Add
                                (
                                    new CodeTableModel()
                                    {
                                        Id = (int)dataReader["EmployeeId"],
                                        Name = String.Format("{0} {1}", dataReader["FirstName"].ToString(), dataReader["LastName"].ToString())
                                    }
                                );
                        }
                    }
                }
                return Ok(responseList);
            }
            catch (Exception e)
            {
                _logger.LogError(String.Format("{0} {1}", e.Message, e.StackTrace));
                return BadRequest();
            }
        }

    }
}
