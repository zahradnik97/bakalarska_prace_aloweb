﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using zaht02_BP.Helpers;

namespace zaht02_BP.Controllers
{
    public abstract class ControllerConfig : ControllerBase
    {
        protected readonly Configuration _config;
        protected readonly ILogger<ControllerConfig> _logger;

        public ControllerConfig(IOptions<Configuration> configAccessor, ILogger<ControllerConfig> logger)
        {
            _config = configAccessor.Value;
            _logger = logger;

        }
    }
}
