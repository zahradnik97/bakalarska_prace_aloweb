﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using zaht02_BP.Helpers;
using zaht02_BP.Models.Employees;
using zaht02_BP.Models.Projects;

namespace zaht02_BP.Controllers
{
    [Route("api/[controller]")]
    [ApiController, Authorize]
    public class ProjectsController : ControllerConfig
    {
        public ProjectsController(IOptions<Configuration> configAccesor, ILogger<ProjectsController> logger) : base(configAccesor, logger)
        {
        }

        // GET: api/Abilities
        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                List<ProjectModel> responseList = new List<ProjectModel>();
                using (SqlConnection connection = new SqlConnection(_config.ConnectionString))
                {
                    connection.Open();
                    string sql = "SELECT * FROM [dbo].Project " +
                        "INNER JOIN [dbo].Employee ON Employee.EmployeeId = Project.ProjectManagerId " +
                        "WHERE ProjectId != @ProjectId;";
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.Add("@ProjectId", SqlDbType.Int).Value = _config.HolidayId;
                        SqlDataReader dataReader = cmd.ExecuteReader();

                        while (dataReader.Read())
                        {
                            responseList.Add
                                (
                                    new ProjectModel()
                                    {
                                        ProjectId = (int)dataReader["ProjectId"],
                                        Name = dataReader["Name"].ToString(),
                                        ProjectManagerId = (int)dataReader["ProjectManagerId"],
                                        Client = dataReader["Client"].ToString(),
                                        ProjectManager =  new EmployeeModel(){
                                            FirstName = dataReader["FirstName"].ToString(),
                                            LastName = dataReader["LastName"].ToString()
                                        },
                                        Note = dataReader["Note"].ToString(),
                                        IsBillable = (bool)dataReader["IsBillable"],
                                        Color = dataReader["Color"].ToString().Trim()
                                    }
                                );
                        }
                    }
                       
                }
                return Ok(responseList);
            }
            catch (Exception e)
            {
                _logger.LogError(String.Format("{0} {1}",e.Message,e.StackTrace));
                return BadRequest();
            }

            
        }

        // GET: api/Abilities
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                ProjectModel responseModel = null;
                using (SqlConnection connection = new SqlConnection(_config.ConnectionString))
                {
                    connection.Open();
                    string sql = String.Format("SELECT * FROM [dbo].Project " +
                        "INNER JOIN [dbo].Employee ON Employee.EmployeeId = Project.ProjectManagerId " +
                        "WHERE ProjectId = @ProjectId", id);
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.Add("@ProjectId", SqlDbType.Int).Value = id;

                        SqlDataReader dataReader = cmd.ExecuteReader();

                        while (dataReader.Read())
                        {
                            responseModel = new ProjectModel()
                                    {
                                        ProjectId = (int)dataReader["ProjectId"],
                                        Name = dataReader["Name"].ToString(),
                                        ProjectManager = new EmployeeModel() {
                                            EmployeeId = (int)dataReader["EmployeeId"],
                                            FirstName = dataReader["FirstName"].ToString(),
                                            LastName = dataReader["LastName"].ToString(),
                                            Position = dataReader["Position"].ToString(),
                                            ContractType = (int)dataReader["ContractType"]
                                        },
                                        Client = dataReader["Client"].ToString(),
                                        Note = dataReader["Note"].ToString(),
                                        IsBillable = (bool)dataReader["IsBillable"],
                                        Color = dataReader["Color"].ToString().Trim()
                            };
                        }
                    }

                }
                return Ok(responseModel);
            }
            catch (Exception e)
            {
                _logger.LogError(String.Format("{0} {1}",e.Message,e.StackTrace));
                return BadRequest();
            }
        }

        // POST: api/Abilities
        [HttpPost, Authorize(Roles = "SalesManager,Administrator")]
        public IActionResult Post([FromBody] ProjectModel newProjectModel)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_config.ConnectionString))
                {
                    connection.Open();
                    var command = new SqlCommand("SELECT COUNT(1) FROM Project WHERE Name = @Name", connection);
                    command.Parameters.Add("@Name", SqlDbType.VarChar).Value = newProjectModel.Name;
                    var nameExists = (int)command.ExecuteScalar() != 0;
                    if (nameExists)
                    {
                        return Ok(new { error = "NAME_EXISTS" });
                    }
                }

                using (SqlConnection connection = new SqlConnection(_config.ConnectionString))
                {
                    connection.Open();
                    string sql = "INSERT INTO [dbo].Project(Name,ProjectManagerId,IsBillable,Note,Color,Client)" +
                        " VALUES(@Name,@ProjectManagerId,@IsBillable,@Note,@Color,@Client)";
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.Add("@Name", SqlDbType.VarChar).Value = newProjectModel.Name;
                        cmd.Parameters.Add("@ProjectManagerId", SqlDbType.Int).Value = newProjectModel.ProjectManagerId;
                        cmd.Parameters.Add("@IsBillable", SqlDbType.Bit).Value = !newProjectModel.IsBillable;
                        cmd.Parameters.Add("@Note", SqlDbType.VarChar).Value = newProjectModel.Note;
                        cmd.Parameters.Add("@Color", SqlDbType.VarChar).Value = newProjectModel.Color;
                        cmd.Parameters.Add("@Client", SqlDbType.VarChar).Value = newProjectModel.Client;
                        cmd.CommandType = CommandType.Text;
                        cmd.ExecuteNonQuery();
                    }
                }
                EmployeeModel identity = Helper.GetIdentityUser(HttpContext.User.Identity as ClaimsIdentity);
                _logger.LogWarning(identity.GetLogInfo("Vytvoření projektu "));
                return Ok(newProjectModel);
            }
            catch (Exception e)
            {
                _logger.LogError(String.Format("{0} {1}",e.Message,e.StackTrace));
                return BadRequest();
            }
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}"), Authorize(Roles = "SalesManager,Administrator")]
        public IActionResult Delete(int id)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_config.ConnectionString))
                {
                    connection.Open();
                    string sql = "DELETE FROM [dbo].Project WHERE ProjectId = @ProjectId";
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.Add("@ProjectId", SqlDbType.Int).Value = id;
                        cmd.CommandType = CommandType.Text;
                        cmd.ExecuteNonQuery();
                    }
                }
                EmployeeModel identity = Helper.GetIdentityUser(HttpContext.User.Identity as ClaimsIdentity);
                _logger.LogWarning(identity.GetLogInfo("DELETE projektu "));
                return Ok();
            }
            catch (Exception e)
            {
                _logger.LogError(String.Format("{0} {1}",e.Message,e.StackTrace));
                return BadRequest();
            }
        }

        [HttpPut,Authorize(Roles = "SalesManager,Administrator")]
        public IActionResult Put([FromBody] ProjectModel updatedModel)
        {
            try
            {
                using(SqlConnection connection = new SqlConnection(_config.ConnectionString))
                {
                    connection.Open();
                    var command = new SqlCommand("SELECT COUNT(1) FROM Project WHERE Name = @Name AND ProjectId != @ProjectId", connection);
                    command.Parameters.Add("@Name", SqlDbType.VarChar).Value = updatedModel.Name;
                    command.Parameters.Add("@ProjectId", SqlDbType.Int).Value = updatedModel.ProjectId;
                    var nameExists = (int)command.ExecuteScalar() != 0;
                    if (nameExists)
                    {
                        return Ok(new { error="NAME_EXISTS" });
                    }
                }

                using (SqlConnection connection = new SqlConnection(_config.ConnectionString))
                {
                    connection.Open();

                    string sql = "UPDATE [dbo].Project SET " +
                        "ProjectManagerId = @ProjectManagerId, " +
                        "Name = @Name, " +
                        "Note = @Note, " +
                        "Color = @Color, " +
                        "Client = @Client, " +
                        "IsBillable = @IsBillable " +
                        "WHERE ProjectId = @ProjectId";
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.Add("@ProjectId", SqlDbType.Int).Value = updatedModel.ProjectId;
                        cmd.Parameters.Add("@ProjectManagerId", SqlDbType.Int).Value = updatedModel.ProjectManagerId;
                        cmd.Parameters.Add("@Name", SqlDbType.VarChar).Value = updatedModel.Name;
                        cmd.Parameters.Add("@IsBillable", SqlDbType.Bit).Value = !updatedModel.IsBillable;
                        cmd.Parameters.Add("@Note", SqlDbType.VarChar).Value = updatedModel.Note;
                        cmd.Parameters.Add("@Color", SqlDbType.VarChar).Value = updatedModel.Color;
                        cmd.Parameters.Add("@Client", SqlDbType.VarChar).Value = updatedModel.Client;
                        cmd.CommandType = CommandType.Text;
                        cmd.ExecuteNonQuery();
                    }
                }
                EmployeeModel identity = Helper.GetIdentityUser(HttpContext.User.Identity as ClaimsIdentity);
                _logger.LogWarning(identity.GetLogInfo("PUT projektu "));
                return Ok(updatedModel);
            }
            catch (Exception e)
            {
                _logger.LogError(String.Format("{0} {1}",e.Message,e.StackTrace));
                return BadRequest();
            }
        }
    }
}