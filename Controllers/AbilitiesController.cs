﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using zaht02_BP.Helpers;
using zaht02_BP.Models.Abilities;
using zaht02_BP.Models.Employees;

namespace zaht02_BP.Controllers
{
    [Route("api/[controller]")]
    [ApiController, Authorize]
    public class AbilitiesController : ControllerConfig
    {
        public AbilitiesController(IOptions<Configuration> configAccesor, ILogger<AbilitiesController> logger) : base(configAccesor, logger)
        {
        }

        // GET: api/Abilities
        [HttpGet, Authorize(Roles = "HRManager,Administrator")] 
        public IActionResult Get()
        {
            try
            {
                List<AbilityModel> responseList = new List<AbilityModel>();
                using (SqlConnection connection = new SqlConnection(_config.ConnectionString))
                {
                    connection.Open();
                    string commandString = "SELECT * FROM [dbo].Ability";
                    using (SqlCommand cmd = new SqlCommand(commandString, connection))
                    {
                        SqlDataReader dataReader = cmd.ExecuteReader();

                        while (dataReader.Read())
                        {
                            responseList.Add
                                (
                                    new AbilityModel()
                                    {
                                        AbilityId = (int)dataReader["abilityId"],
                                        Name = dataReader["name"].ToString()
                                    }
                                );
                        }
                    }
                }
                return Ok(responseList);
            }
            catch (Exception e)
            {
                _logger.LogError(String.Format("{0} {1}",e.Message,e.StackTrace));
                return BadRequest();
            }
            

        }

        // POST: api/Abilities
        [HttpPost, Authorize(Roles = "HRManager,Administrator")]
        public IActionResult Post([FromBody] AbilityModel newAbilityModel)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_config.ConnectionString))
                {
                    connection.Open();
                    string sql = "INSERT INTO [dbo].Ability(name) VALUES(@name)";
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.Add("@name", SqlDbType.VarChar).Value = newAbilityModel.Name;
                        cmd.CommandType = CommandType.Text;
                        cmd.ExecuteNonQuery();
                    }
                }
                EmployeeModel identity = Helper.GetIdentityUser(HttpContext.User.Identity as ClaimsIdentity);
                _logger.LogWarning(identity.GetLogInfo("Vytvoření schopnosti "));
                return Ok(newAbilityModel);
            }
            catch (Exception e)
            {
                _logger.LogError(String.Format("{0} {1}", e.Message, e.StackTrace));
                return BadRequest();
            }
        }

        // DELETE: api/Abilities/id
        [HttpDelete("{id}"), Authorize(Roles = "HRManager,Administrator")]
        public IActionResult Delete(int id)
        {

            try
            {
                using (SqlConnection connection = new SqlConnection(_config.ConnectionString))
                {
                    connection.Open();
                    string sql = "DELETE FROM [dbo].Ability WHERE abilityId = @abilityId";
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.Add("@abilityId", SqlDbType.Int).Value = id;
                        cmd.CommandType = CommandType.Text;
                        cmd.ExecuteNonQuery();
                    }
                }
                EmployeeModel identity = Helper.GetIdentityUser(HttpContext.User.Identity as ClaimsIdentity);
                _logger.LogWarning(identity.GetLogInfo("DELETE schopnosti "));
                return Ok();
            }
            catch (Exception e)
            {
                _logger.LogError(String.Format("{0} {1}", e.Message, e.StackTrace));
                return BadRequest();
            }
        }

        // PUT: api/Abilities
        [HttpPut, Authorize(Roles = "HRManager,Administrator")]
        public IActionResult Put([FromBody] AbilityModel updatedModel)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_config.ConnectionString))
                {
                    connection.Open();
                    string sql = "UPDATE [dbo].Ability SET name = @name WHERE abilityId = @abilityId";
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.Add("@abilityId", SqlDbType.Int).Value = updatedModel.AbilityId;
                        cmd.Parameters.Add("@name", SqlDbType.VarChar).Value = updatedModel.Name;
                        cmd.CommandType = CommandType.Text;
                        cmd.ExecuteNonQuery();
                    }
                }
                EmployeeModel identity = Helper.GetIdentityUser(HttpContext.User.Identity as ClaimsIdentity);
                _logger.LogWarning(identity.GetLogInfo("PUT schopnosti "));
                return Ok(updatedModel);
            }
            catch (Exception e)
            {
                _logger.LogError(String.Format("{0} {1}", e.Message, e.StackTrace));
                return BadRequest();
            }
        }
    }
}
