﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using zaht02_BP.Helpers;
using zaht02_BP.Models.Abilities;
using zaht02_BP.Models.Employees;

namespace zaht02_BP.Controllers
{
    [Route("api/[controller]")]
    [ApiController, Authorize]
    public class EmployeesController : ControllerConfig
    {
        public EmployeesController(IOptions<Configuration> configAccesor, ILogger<EmployeesController> logger) : base(configAccesor, logger)
        {
        }

        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                List<EmployeeModel> responseList = new List<EmployeeModel>();
                using (SqlConnection connection = new SqlConnection(_config.ConnectionString))
                {
                    connection.Open();
                    string sql = "SELECT * FROM [dbo].Employee";
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        SqlDataReader dataReader = cmd.ExecuteReader();
                        while (dataReader.Read())
                        {
                            responseList.Add
                                (
                                    new EmployeeModel()
                                    {
                                        EmployeeId = (int)dataReader["EmployeeId"],
                                        FirstName = dataReader["FirstName"].ToString(),
                                        LastName = dataReader["LastName"].ToString(),
                                        Position = dataReader["Position"].ToString(),
                                        Email = dataReader["Email"].ToString()
                                    }
                                ); ;
                        }
                        dataReader.Close();
                    }
                    responseList.ForEach((item) => { item.Abilities = GetEmployeeAbilities(item.EmployeeId, connection); });
                }
                return Ok(responseList);
            }
            catch (Exception e)
            {
                _logger.LogError(String.Format("{0} {1}", e.Message, e.StackTrace));
                return BadRequest();
            }
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            EmployeeModel responseModel = null;
            try
            {
                using (SqlConnection connection = new SqlConnection(_config.ConnectionString))
                {
                    connection.Open();
                    string sql = "SELECT * FROM [dbo].Employee WHERE EmployeeId = @EmployeeId";
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.Add("@EmployeeId", SqlDbType.Int).Value = id;

                        SqlDataReader dataReader = cmd.ExecuteReader();

                        while (dataReader.Read())
                        {
                            responseModel = new EmployeeModel()
                            {
                                EmployeeId = (int)dataReader["EmployeeId"],
                                FirstName = dataReader["FirstName"].ToString(),
                                LastName = dataReader["LastName"].ToString(),
                                Position = dataReader["Position"].ToString(),
                                ContractType = (int)dataReader["ContractType"],
                                Email = dataReader["Email"].ToString()
                            };
                        }
                        dataReader.Close();
                    }
                    responseModel.Abilities = GetEmployeeAbilities(responseModel.EmployeeId, connection);
                    responseModel.Roles = GetEmployeeRoles(responseModel.EmployeeId, connection);
                }
                return Ok(responseModel);
            }
            catch (Exception e)
            {
                _logger.LogError(String.Format("{0} {1}", e.Message, e.StackTrace));
                return BadRequest();
            }
        }

        private AbilityModel[] GetEmployeeAbilities(int employeeId, SqlConnection connection)
        {

            try
            {
                List<AbilityModel> returnValue = new List<AbilityModel>();

                string sql = "SELECT Ability.AbilityId, Ability.Name FROM [dbo].AbilityEmployee " +
                    "INNER JOIN [dbo].Ability " +
                    "ON AbilityEmployee.AbilityId = Ability.AbilityId" +
                    " WHERE AbilityEmployee.EmployeeId = @EmployeeId";
                using (SqlCommand cmd = new SqlCommand(sql, connection))
                {
                    cmd.Parameters.Add("@EmployeeId", SqlDbType.Int).Value = employeeId;
                    SqlDataReader dataReader = cmd.ExecuteReader();

                    while (dataReader.Read())
                    {
                        returnValue.Add(
                            new AbilityModel
                            {
                                AbilityId = (int)dataReader["AbilityId"],
                                Name = dataReader["Name"].ToString()
                            });
                    }
                    dataReader.Close();
                }
                return returnValue?.ToArray();
            }
            catch (Exception e)
            {
                _logger.LogError(String.Format("{0} {1}", e.Message, e.StackTrace));
                return null;
            }
        }
        private string[] GetEmployeeRoles(int employeeId, SqlConnection connection)
        {
            try
            {
                List<string> returnValue = new List<string>();
                string sql = "SELECT EmployeeId, Role FROM [dbo].Role " +
                    " WHERE EmployeeId = @EmployeeId";
                using (SqlCommand cmd = new SqlCommand(sql, connection))
                {
                    cmd.Parameters.Add("@EmployeeId", SqlDbType.Int).Value = employeeId;
                    SqlDataReader dataReader = cmd.ExecuteReader();

                    while (dataReader.Read())
                    {
                        returnValue.Add(dataReader["Role"].ToString());
                    }
                }
                return returnValue?.ToArray();
            }
            catch (Exception e)
            {
                _logger.LogError(String.Format("{0} {1}", e.Message, e.StackTrace));
                return null;
            }
        }

        [HttpPost, Authorize(Roles = "HRManager,Administrator")]
        public IActionResult Post([FromBody] EmployeeModel newEmployeeModel)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_config.ConnectionString))
                {
                    connection.Open();
                    var command = new SqlCommand("SELECT COUNT(1) FROM Employee WHERE Email = @Email", connection);
                    command.Parameters.Add("@Email", SqlDbType.VarChar).Value = newEmployeeModel.Email;

                    var emailExists = (int)command.ExecuteScalar() != 0;
                    if (emailExists)
                    {
                        return Ok(new { error = "EMAIL_EXISTS" });
                    }
                }

                using (SqlConnection connection = new SqlConnection(_config.ConnectionString))
                {
                    connection.Open();
                    string sql = "INSERT INTO [dbo].Employee(FirstName,LastName,Position,ContractType,Email)" +
                        " VALUES(@FirstName,@LastName,@Position,@ContractType,@Email);" +
                        " SELECT SCOPE_IDENTITY()";
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {

                        cmd.Parameters.Add("@FirstName", SqlDbType.VarChar).Value = newEmployeeModel.FirstName;
                        cmd.Parameters.Add("@LastName", SqlDbType.VarChar).Value = newEmployeeModel.LastName;
                        cmd.Parameters.Add("@Position", SqlDbType.VarChar).Value = newEmployeeModel.Position;
                        cmd.Parameters.Add("@ContractType", SqlDbType.Int).Value = newEmployeeModel.ContractType;
                        cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = newEmployeeModel.Email;

                        cmd.CommandType = CommandType.Text;
                        decimal newEmployeeId = (decimal)cmd.ExecuteScalar();
                        newEmployeeModel.EmployeeId = Decimal.ToInt32(newEmployeeId);

                    }
                    RegisterUser(newEmployeeModel, connection);
                    ModifyEmployeeAbilities(newEmployeeModel, connection);
                    ModifyEmployeeRoles(newEmployeeModel, connection);
                }
                EmployeeModel identity = Helper.GetIdentityUser(HttpContext.User.Identity as ClaimsIdentity);
                _logger.LogWarning(identity.GetLogInfo("Vytvoření zaměstnance "));
                return Ok(newEmployeeModel);
            }
            catch (Exception e)
            {
                _logger.LogError(String.Format("{0} {1}", e.Message, e.StackTrace));
                return BadRequest();
            }
        }

        private void RegisterUser(EmployeeModel newEmployeeModel, SqlConnection connection)
        {
            try
            {
                newEmployeeModel.Username = (newEmployeeModel.FirstName[0] + Helper.RemoveDiacritism(newEmployeeModel.LastName)).ToLower();

                var command = new SqlCommand(String.Format("SELECT COUNT(*) FROM Employee WHERE FirstName LIKE '{0}%' AND LastName = @LastName", newEmployeeModel.FirstName[0]), connection);
                command.Parameters.Add("@LastName", SqlDbType.VarChar).Value = newEmployeeModel.LastName;

                int usernameCount = (int)command.ExecuteScalar() - 1;
                if (usernameCount > 0)
                {
                    newEmployeeModel.Username += usernameCount.ToString();

                }

                string generatedPassword = Helper.RandomPassword();
                string sqlUser = "INSERT INTO [dbo].[User](Username,EmployeeId,Password)" +
                    " VALUES(@Username,@EmployeeId,@Password);";
                using (SqlCommand cmd = new SqlCommand(sqlUser, connection))
                {
                    cmd.Parameters.Add("@Username", SqlDbType.VarChar).Value = newEmployeeModel.Username;
                    cmd.Parameters.Add("@EmployeeId", SqlDbType.Int).Value = newEmployeeModel.EmployeeId;
                    cmd.Parameters.Add("@Password", SqlDbType.VarChar).Value = Helper.Hash(generatedPassword);

                    cmd.CommandType = CommandType.Text;
                    cmd.ExecuteNonQuery();

                }

                SendRegistrationEmail(generatedPassword, newEmployeeModel);
            }
            catch (Exception e)
            {
                _logger.LogError(String.Format("{0} {1}", e.Message, e.StackTrace));
            }
        }

        private void SendRegistrationEmail(string password, EmployeeModel employee)
        {
            try
            {
                var message = "Dobrý den,<br> prosím použijte následující údaje k jednorázovému přihlášení." +
                    " Ihned si v aplikaci změnte následující heslo! <br> přihlašovací jméno: " + employee.Username + " <br> heslo: " + password;
                var subject = "Vaše jednorázové přihlašovací údaje";
                Helper.SendEmail(_config,employee.Email,message,subject);
            }
            catch (Exception e)
            {
                _logger.LogError(String.Format("{0} {1}", e.Message, e.StackTrace));
            }
        }

        private void ModifyEmployeeRoles(EmployeeModel employee, SqlConnection connection)
        {

            try
            {
                string sqlDelete = String.Format("DELETE FROM [dbo].Role WHERE EmployeeId = @EmployeeId", employee.EmployeeId);
                using (SqlCommand cmd = new SqlCommand(sqlDelete, connection))
                {
                    cmd.Parameters.Add("@EmployeeId", SqlDbType.Int).Value = employee.EmployeeId;
                    cmd.CommandType = CommandType.Text;
                    cmd.ExecuteNonQuery();
                }
                if (employee.Abilities != null && employee.Abilities.Length > 0)
                {
                    string sqlInsert = "INSERT INTO [dbo].Role(Role,EmployeeId)" +
                   " VALUES(@Role,@EmployeeId)";

                    if (employee.Roles.Length == 0)
                    {
                        employee.Roles = new string[] {"Employee" };
                    }

                    employee.Roles.ToList().ForEach((role) =>
                    {

                        using (SqlCommand cmd = new SqlCommand(sqlInsert, connection))
                        {
                            cmd.Parameters.Add("@Role", SqlDbType.VarChar).Value = role;
                            cmd.Parameters.Add("@EmployeeId", SqlDbType.Int).Value = employee.EmployeeId;

                            cmd.CommandType = CommandType.Text;
                            cmd.ExecuteNonQuery();
                        }
                    });
                }

            }
            catch (Exception e)
            {
                _logger.LogError(String.Format("{0} {1}", e.Message, e.StackTrace));
            }
        }

        [HttpDelete("{id}"), Authorize(Roles = "HRManager,Administrator")]
        public IActionResult Delete(int id)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_config.ConnectionString))
                {
                    connection.Open();
                    string sql = "DELETE FROM [dbo].Employee WHERE EmployeeId = @EmployeeId";
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.Add("@EmployeeId", SqlDbType.Int).Value = id;
                        cmd.CommandType = CommandType.Text;
                        cmd.ExecuteNonQuery();
                    }
                }
                EmployeeModel identity = Helper.GetIdentityUser(HttpContext.User.Identity as ClaimsIdentity);
                _logger.LogWarning(identity.GetLogInfo("DELETE zaměstnance "));
                return Ok();
            }
            catch (Exception e)
            {
                _logger.LogError(String.Format("{0} {1}", e.Message, e.StackTrace));
                return BadRequest();
            }
        }

        [HttpPut, Authorize(Roles = "HRManager,Administrator")]
        public IActionResult Put([FromBody] EmployeeModel updatedModel)
        {

            try
            {
                using (SqlConnection connection = new SqlConnection(_config.ConnectionString))
                {

                    connection.Open();
                    var command = new SqlCommand("SELECT COUNT(1) FROM Employee WHERE Email = @Email AND EmployeeId != @EmployeeId", connection);
                    command.Parameters.Add("@Email", SqlDbType.VarChar).Value = updatedModel.Email;
                    command.Parameters.Add("@EmployeeId", SqlDbType.Int).Value = updatedModel.EmployeeId;
                    var emailExists = (int)command.ExecuteScalar() != 0;
                    if (emailExists)
                    {
                        return Ok(new { error = "EMAIL_EXISTS" });
                    }
                }

                using (SqlConnection connection = new SqlConnection(_config.ConnectionString))
                {
                    connection.Open();
                    string sql = "UPDATE [dbo].Employee SET " +
                        "FirstName = @FirstName, " +
                        "LastName = @LastName, " +
                        "Position = @Position, " +
                        "ContractType = @ContractType, " +
                        "Email = @Email " +
                        "WHERE EmployeeId = @EmployeeId";
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.Add("@EmployeeId", SqlDbType.Int).Value = updatedModel.EmployeeId;

                        cmd.Parameters.Add("@FirstName", SqlDbType.VarChar).Value = updatedModel.FirstName;
                        cmd.Parameters.Add("@LastName", SqlDbType.VarChar).Value = updatedModel.LastName;
                        cmd.Parameters.Add("@Position", SqlDbType.VarChar).Value = updatedModel.Position;
                        cmd.Parameters.Add("@ContractType", SqlDbType.Int).Value = updatedModel.ContractType;
                        cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = updatedModel.Email;
                        cmd.CommandType = CommandType.Text;
                        cmd.ExecuteNonQuery();


                    }
                    ModifyEmployeeAbilities(updatedModel, connection);
                    ModifyEmployeeRoles(updatedModel, connection);


                }
                EmployeeModel identity = Helper.GetIdentityUser(HttpContext.User.Identity as ClaimsIdentity);
                _logger.LogWarning(identity.GetLogInfo("PUT zaměstnance "));
                return Ok(updatedModel);
            }
            catch (Exception e)
            {
                _logger.LogError(String.Format("{0} {1}", e.Message, e.StackTrace));
                return BadRequest();
            }

        }

        private void ModifyEmployeeAbilities(EmployeeModel employee, SqlConnection connection)
        {

            try
            {
                string sqlDelete = "DELETE FROM [dbo].AbilityEmployee WHERE EmployeeId = @EmployeeId";
                using (SqlCommand cmd = new SqlCommand(sqlDelete, connection))
                {
                    cmd.Parameters.Add("@EmployeeId", SqlDbType.Int).Value = employee.EmployeeId;
                    cmd.CommandType = CommandType.Text;
                    cmd.ExecuteNonQuery();
                }
                if (employee.Abilities != null && employee.Abilities.Length > 0)
                {
                    string sqlInsert = "INSERT INTO [dbo].AbilityEmployee(AbilityId,EmployeeId)" +
                   " VALUES(@AbilityId,@EmployeeId)";

                    employee.Abilities.ToList().ForEach((ability) =>
                    {

                        using (SqlCommand cmd = new SqlCommand(sqlInsert, connection))
                        {
                            cmd.Parameters.Add("@AbilityId", SqlDbType.Int).Value = ability.AbilityId;
                            cmd.Parameters.Add("@EmployeeId", SqlDbType.Int).Value = employee.EmployeeId;

                            cmd.CommandType = CommandType.Text;
                            cmd.ExecuteNonQuery();
                        }
                    });
                }

            }
            catch (Exception e)
            {
                _logger.LogError(String.Format("{0} {1}", e.Message, e.StackTrace));
            }

        }
    }
}