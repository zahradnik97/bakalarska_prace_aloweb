﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using NLog;
using zaht02_BP.Helpers;
using zaht02_BP.Models.Abilities;
using zaht02_BP.Models.Auth;
using zaht02_BP.Models.Employees;

namespace zaht02_BP.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerConfig
    {
        public AuthController(IOptions<Configuration> configAccesor, ILogger<AuthController> logger) : base(configAccesor, logger)
        {
        }

        [HttpGet("[action]"), Authorize]
        public IActionResult Authorize()
        {
            EmployeeModel identity = Helper.GetIdentityUser(HttpContext.User.Identity as ClaimsIdentity);
            return Ok(identity);
        }

        [HttpPost("[action]")]
        public IActionResult Login([FromBody]LoginModel loginRequest)
        {
            LoginModel databaseModel = null;
            EmployeeModel employee = null;
            bool passwordChanged = false;
            try
            {
                using (SqlConnection connection = new SqlConnection(_config.ConnectionString))
                {
                    connection.Open();
                    string sql = "SELECT * FROM [dbo].[User] " +
                    "WHERE Username = @Username;";
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.Add("@Username", SqlDbType.VarChar).Value = loginRequest.Username;
                        SqlDataReader dataReader = cmd.ExecuteReader();

                        while (dataReader.Read())
                        {
                            databaseModel = new LoginModel()
                            {
                                Password = dataReader["Password"].ToString(),
                                Username = dataReader["Username"].ToString(),
                            };
                            passwordChanged = (bool)dataReader["PasswordChanged"];

                        }
                        dataReader.Close();
                    }

                }

            }
            catch (Exception e)
            {
                _logger.LogError(String.Format("{0} {1}", e.Message, e.StackTrace));
            }

            if (databaseModel != null && databaseModel?.Password == Helper.Hash(loginRequest.Password))
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(_config.ConnectionString))
                    {
                        connection.Open();
                        string sql = "SELECT * FROM [dbo].[User] " +
                    "INNER JOIN [dbo].Employee ON [dbo].[User].EmployeeId = [dbo].[Employee].EmployeeId " +
                    "WHERE Username = @Username;";
                        using (SqlCommand cmd = new SqlCommand(sql, connection))
                        {
                            cmd.Parameters.Add("@Username", SqlDbType.VarChar).Value = loginRequest.Username;
                            SqlDataReader dataReader = cmd.ExecuteReader();

                            while (dataReader.Read())
                            {
                                employee = new EmployeeModel()
                                {
                                    EmployeeId = (int)dataReader["EmployeeId"],
                                    FirstName = dataReader["FirstName"].ToString(),
                                    LastName = dataReader["LastName"].ToString(),
                                    Position = dataReader["Position"].ToString(),
                                    ContractType = (int)dataReader["ContractType"],
                                    Email = dataReader["Email"].ToString()
                                };

                            }
                            dataReader.Close();
                            employee.Abilities = GetEmployeeAbilities(employee.EmployeeId, connection);
                            employee.Roles = GetEmployeeRoles(employee.EmployeeId, connection);
                        }
                    }

                    var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config.Secret));
                    var signinCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);
                    List<Claim> claims = new List<Claim> { new Claim(ClaimTypes.UserData,
                    Newtonsoft.Json.JsonConvert.SerializeObject(employee)) };
                    using (SqlConnection connection = new SqlConnection(_config.ConnectionString))
                    {
                        connection.Open();
                        string sql = "SELECT * FROM [dbo].[Role] WHERE EmployeeId = @EmployeeId;";
                        using (SqlCommand cmd = new SqlCommand(sql, connection))
                        {
                            cmd.Parameters.Add("@EmployeeId", SqlDbType.Int).Value = employee.EmployeeId;
                            SqlDataReader dataReader = cmd.ExecuteReader();

                            while (dataReader.Read())
                            {
                                claims.Add(new Claim(ClaimTypes.Role, dataReader["Role"].ToString()));
                            }
                            dataReader.Close();
                        }

                    }

                    var tokeOptions = new JwtSecurityToken(
                        issuer: _config.Url,
                        audience: _config.Url,
                        claims: claims,
                        expires: DateTime.Now.AddMinutes(30),
                        signingCredentials: signinCredentials
                    );

                    var tokenString = new JwtSecurityTokenHandler().WriteToken(tokeOptions);
                    _logger.LogWarning("Přihlášení uživatele " + employee.Username);
                    return Ok(new { User = employee, Token = tokenString, PasswordChanged = passwordChanged });
                }
                catch (Exception e)
                {
                    _logger.LogError(String.Format("{0} {1}", e.Message, e.StackTrace));
                    return BadRequest();
                }
            }
            else
            {
                return Unauthorized();
            }
        }
        private AbilityModel[] GetEmployeeAbilities(int employeeId, SqlConnection connection)
        {
            try
            {
                List<AbilityModel> returnValue = new List<AbilityModel>();

                string sql = "SELECT Ability.AbilityId, Ability.Name FROM [dbo].AbilityEmployee " +
                    "INNER JOIN [dbo].Ability " +
                    "ON AbilityEmployee.AbilityId = Ability.AbilityId" +
                    " WHERE AbilityEmployee.EmployeeId = @EmployeeId";
                using (SqlCommand cmd = new SqlCommand(sql, connection))
                {
                    cmd.Parameters.Add("@EmployeeId", SqlDbType.Int).Value = employeeId;
                    SqlDataReader dataReader = cmd.ExecuteReader();

                    while (dataReader.Read())
                    {
                        returnValue.Add(
                            new AbilityModel
                            {
                                AbilityId = (int)dataReader["AbilityId"],
                                Name = dataReader["Name"].ToString()
                            });
                    }
                    dataReader.Close();
                }

                return returnValue?.ToArray();
            }
            catch (Exception e)
            {
                _logger.LogError(String.Format("{0} {1}", e.Message, e.StackTrace));
                return null;
            }
        }
        private string[] GetEmployeeRoles(int employeeId, SqlConnection connection)
        {
            try
            {
                List<string> returnValue = new List<string>();
                string sql = "SELECT EmployeeId, Role FROM [dbo].Role " +
                    " WHERE EmployeeId = @EmployeeId";
                using (SqlCommand cmd = new SqlCommand(sql, connection))
                {
                    cmd.Parameters.Add("@EmployeeId", SqlDbType.Int).Value = employeeId;
                    SqlDataReader dataReader = cmd.ExecuteReader();

                    while (dataReader.Read())
                    {
                        returnValue.Add(dataReader["Role"].ToString());
                    }
                }
                if (returnValue.Count == 0)
                {
                    returnValue.Add("Employee");
                }
                return returnValue?.ToArray();
            }
            catch (Exception e)
            {
                _logger.LogError(String.Format("{0} {1}", e.Message, e.StackTrace));
                return null;
            }
        }

        [HttpPut("[action]"), Authorize]
        public IActionResult ChangePassword([FromBody]ChangePasswordModel changePasswordModel)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_config.ConnectionString))
                {
                    LoginModel databaseModel = null;
                    connection.Open();
                    string sqlGet = "SELECT * FROM [dbo].[User] WHERE EmployeeId = @EmployeeId;";
                    using (SqlCommand cmd = new SqlCommand(sqlGet, connection))
                    {
                        cmd.Parameters.Add("@EmployeeId", SqlDbType.Int).Value = changePasswordModel.EmployeeId;
                        SqlDataReader dataReader = cmd.ExecuteReader();

                        while (dataReader.Read())
                        {
                            databaseModel = new LoginModel()
                            {
                                Password = dataReader["Password"].ToString(),
                                Username = dataReader["Username"].ToString(),
                            };
                        }
                        dataReader.Close();
                    }

                    if (databaseModel != null && databaseModel.Password == Helper.Hash(changePasswordModel.OldPassword))
                    {
                        string sqlUpdate = "UPDATE [dbo].[User] SET " +
                            "Password = @Password, PasswordChanged = @PasswordChanged WHERE EmployeeId = @EmployeeId";
                        using (SqlCommand cmd = new SqlCommand(sqlUpdate, connection))
                        {
                            cmd.Parameters.Add("@EmployeeId", SqlDbType.Int).Value = changePasswordModel.EmployeeId;

                            cmd.Parameters.Add("@Password", SqlDbType.VarChar).Value = Helper.Hash(changePasswordModel.NewPassword);
                            cmd.Parameters.Add("@PasswordChanged", SqlDbType.Bit).Value = true;
                            cmd.CommandType = CommandType.Text;
                            cmd.ExecuteNonQuery();
                        }
                        EmployeeModel identity = Helper.GetIdentityUser(HttpContext.User.Identity as ClaimsIdentity);
                        _logger.LogWarning(identity.GetLogInfo("Změna hesla "));
                        return Ok(new { message = "success" });
                    }
                    else
                    {
                        return Ok(new { message = "wrong_password" });
                    }

                }
            }
            catch (Exception e)
            {
                _logger.LogError(String.Format("{0} {1}", e.Message, e.StackTrace));
                return BadRequest();
            }

        }

        [HttpPut("[action]/{employeeId}"), Authorize(Roles = "HRManager,Administrator")]
        public IActionResult ResetPassword(int employeeId)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_config.ConnectionString))
                {
                    connection.Open();
                    var generatedPassword = Helper.RandomPassword();
                    string sqlUpdate = "UPDATE [dbo].[User] SET " +
                            "Password = @Password, PasswordChanged = @PasswordChanged WHERE EmployeeId = @EmployeeId";
                    using (SqlCommand cmd = new SqlCommand(sqlUpdate, connection))
                    {
                        cmd.Parameters.Add("@EmployeeId", SqlDbType.Int).Value = employeeId;

                        cmd.Parameters.Add("@Password", SqlDbType.VarChar).Value = Helper.Hash(generatedPassword);
                        cmd.Parameters.Add("@PasswordChanged", SqlDbType.Bit).Value = false;
                        cmd.CommandType = CommandType.Text;
                        cmd.ExecuteNonQuery();
                    }

                    try
                    {

                        EmployeeModel employee = null;
                        string getEmailSql = "SELECT Email, [User].Username FROM Employee " +
                            "INNER JOIN [dbo].[User] " +
                            "ON [User].EmployeeId = Employee.EmployeeId " +
                            "WHERE Employee.EmployeeId = @EmployeeId;";
                        using (SqlCommand cmd = new SqlCommand(getEmailSql, connection))
                        {
                            cmd.Parameters.Add("@EmployeeId", SqlDbType.Int).Value = employeeId;
                            SqlDataReader dataReader = cmd.ExecuteReader();

                            while (dataReader.Read())
                            {
                                employee = new EmployeeModel()
                                {
                                    Email = dataReader["Email"].ToString(),
                                    Username = dataReader["Username"].ToString(),
                                };
                            }
                            dataReader.Close();
                        }

                        Helper.SendEmail(_config
                            , employee.Email
                            , String.Format("Vaše nově vygenerované údaje <br> přihlašovací jméno: {0} <br> heslo: {1}", employee.Username, generatedPassword)
                            , "Nově vygenerované heslo");
                    }
                    catch (Exception e)
                    {
                        _logger.LogError(String.Format("{0} {1}", e.Message, e.StackTrace));
                    }
                    EmployeeModel identity = Helper.GetIdentityUser(HttpContext.User.Identity as ClaimsIdentity);
                    _logger.LogWarning(identity.GetLogInfo("Reset hesla "));
                    return Ok();
                }
            }
            catch (Exception e)
            {
                _logger.LogError(String.Format("{0} {1}", e.Message, e.StackTrace));
                return BadRequest();
            }

        }
    }
}