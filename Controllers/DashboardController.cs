﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using zaht02_BP.Helpers;
using zaht02_BP.Models.Abilities;
using zaht02_BP.Models.Dashboard;
using zaht02_BP.Models.Employees;
using zaht02_BP.Models.Projects;

namespace zaht02_BP.Controllers
{
    [Route("api/[controller]")]
    [ApiController, Authorize]
    public class DashboardController : ControllerConfig
    {
        public DashboardController(IOptions<Configuration> configAccesor, ILogger<DashboardController> logger) : base(configAccesor, logger)
        {
        }

        [HttpGet("{weekId:int}")]
        public IActionResult Get(int weekId)
        {
            try
            {
                string[] days = new string[5];
                for (int i = 0; i < 5; ++i)
                    days[i] = weekId.ToString() + (i + 1).ToString();
                List<int> employeesIds = new List<int>(GetEmployees());

                List<DashboardModel> feResponseList = new List<DashboardModel>();
                employeesIds.ForEach((employeeId) =>
                {
                    feResponseList.Add(
                            new DashboardModel()
                            {
                                Employee = GetEmployee(employeeId),
                                Week = new WeekModel(weekId, days, employeeId, _config.ConnectionString),
                            }
                        );
                });

                return Ok(feResponseList);
            }
            catch (Exception e)
            {
                _logger.LogError(String.Format("{0} {1}", e.Message, e.StackTrace));
                return BadRequest();
            }
        }

        private List<int> GetEmployees()
        {
            try
            {
                List<int> response = new List<int>();
                using (SqlConnection connection = new SqlConnection(_config.ConnectionString))
                {
                    connection.Open();
                    string sql = "SELECT EmployeeId FROM [dbo].[Employee]";
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        SqlDataReader dataReader = cmd.ExecuteReader();

                        while (dataReader.Read())
                        {
                            response.Add(
                                (int)dataReader["EmployeeId"]
                                );
                        }
                    }
                }
                return response;
            }
            catch (Exception e)
            {
                _logger.LogError(String.Format("{0} {1}", e.Message, e.StackTrace));
                return null;
            }

        }

        private EmployeeModel GetEmployee(int employeeId)
        {
            try
            {
                EmployeeModel responseModel = null;
                using (SqlConnection connection = new SqlConnection(_config.ConnectionString))
                {
                    connection.Open();
                    string sql = "SELECT * FROM [dbo].Employee WHERE EmployeeId = @EmployeeId";
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.Add("@EmployeeId", SqlDbType.Int).Value = employeeId;
                        SqlDataReader dataReader = cmd.ExecuteReader();

                        while (dataReader.Read())
                        {
                            responseModel = new EmployeeModel()
                            {
                                EmployeeId = (int)dataReader["EmployeeId"],
                                FirstName = dataReader["FirstName"].ToString(),
                                LastName = dataReader["LastName"].ToString(),
                                Position = dataReader["Position"].ToString(),
                                ContractType = (int)dataReader["ContractType"]
                            };
                        }
                    }

                }
                responseModel.Abilities = GetEmployeeAbilities(responseModel.EmployeeId);
                return responseModel;
            }
            catch (Exception e)
            {
                _logger.LogError(String.Format("{0} {1}", e.Message, e.StackTrace));
                return null;
            }


        }

        private AbilityModel[] GetEmployeeAbilities(int employeeId)
        {
            try
            {
                List<AbilityModel> returnValue = new List<AbilityModel>();
                using (SqlConnection connection = new SqlConnection(_config.ConnectionString))
                {
                    connection.Open();
                    string sql = "SELECT Ability.AbilityId, Ability.Name FROM [dbo].AbilityEmployee " +
                        "INNER JOIN [dbo].Ability " +
                        "ON AbilityEmployee.AbilityId = Ability.AbilityId" +
                        " WHERE AbilityEmployee.EmployeeId = @EmployeeId";
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.Add("@EmployeeId", SqlDbType.Int).Value = employeeId;

                        SqlDataReader dataReader = cmd.ExecuteReader();

                        while (dataReader.Read())
                        {
                            returnValue.Add(
                                new AbilityModel
                                {
                                    AbilityId = (int)dataReader["AbilityId"],
                                    Name = dataReader["Name"].ToString()
                                });
                        }
                    }
                }
                return returnValue?.ToArray();
            }
            catch (Exception e)
            {
                _logger.LogError(String.Format("{0} {1}", e.Message, e.StackTrace));
                return null;
            }

        }

        [HttpPut, Authorize(Roles = "ProjectManager,Administrator")]
        public IActionResult Put([FromBody] DashboardResponseModel[] dashboardModel)
        {

            try
            {
                //int contractType = 0;
                //using (SqlConnection connection = new SqlConnection(_config.ConnectionString))
                //{
                //    connection.Open();
                //    string sqlDelete = "SELECT ContractType FROM [dbo].Employee " +
                //        "WHERE EmployeeId = @EmployeeId";
                //    SqlCommand cmd = new SqlCommand(sqlDelete, connection);
                //    cmd.Parameters.Add("@EmployeeId", SqlDbType.Int).Value = dashboardModel[0].EmployeeId;
                //    contractType = (int)cmd.ExecuteScalar();
                //}

                //var countOfAllocation = 0;
                //dashboardModel.ToList().ForEach((item) =>
                //{
                //    countOfAllocation += item.Allocation;
                //});
                //_logger.LogWarning(String.Format("{0} {1}",countOfAllocation,contractType));
                //if (countOfAllocation > contractType)
                //{
                //    return BadRequest();
                //}
                //else
                //{
                EmployeeModel identity = Helper.GetIdentityUser(HttpContext.User.Identity as ClaimsIdentity);

                _logger.LogWarning(identity.GetLogInfo("Pokus o PUT alokace "));

                List<DashboardResponseModel> finalModel = identity.Roles.Contains("Administrator") ?
                    dashboardModel.ToList() :
                    Helper.CanAllocateProject(_config.ConnectionString, dashboardModel, identity.EmployeeId);

                finalModel.ForEach((item) =>
                {

                    using (SqlConnection connection = new SqlConnection(_config.ConnectionString))
                    {
                        connection.Open();
                        string sqlDelete = "DELETE FROM [dbo].DayProjectEmployee " +
                            "WHERE EmployeeId = @EmployeeId AND DayId = @DayId AND ProjectId = @ProjectId";
                        using (SqlCommand cmd = new SqlCommand(sqlDelete, connection))
                        {
                            cmd.Parameters.Add("@EmployeeId", SqlDbType.Int).Value = item.EmployeeId;
                            cmd.Parameters.Add("@DayId", SqlDbType.Int).Value = item.DayId;
                            cmd.Parameters.Add("@ProjectId", SqlDbType.Int).Value = item.ProjectId;
                            cmd.ExecuteNonQuery();
                        }
                    }

                });

                finalModel.ForEach((item) =>
                {
                    if (item.Allocation > 0)
                    {

                        using (SqlConnection connection = new SqlConnection(_config.ConnectionString))
                        {
                            connection.Open();

                            string sqlInsert = "INSERT INTO [dbo].DayProjectEmployee(EmployeeId,ProjectId,DayId,Allocation, ConfirmedRequest)" +
                            " VALUES(@EmployeeId,@ProjectId,@DayId,@Allocation, @ConfirmedRequest);";
                            using (SqlCommand cmd = new SqlCommand(sqlInsert, connection))
                            {
                                cmd.Parameters.Add("@DayId", SqlDbType.Int).Value = item.DayId;
                                cmd.Parameters.Add("@ProjectId", SqlDbType.Int).Value = item.ProjectId;
                                cmd.Parameters.Add("@Allocation", SqlDbType.Decimal).Value = item.Allocation;
                                cmd.Parameters.Add("@EmployeeId", SqlDbType.Int).Value = item.EmployeeId;
                                cmd.Parameters.Add("@ConfirmedRequest", SqlDbType.Int).Value = 0;

                                cmd.CommandType = CommandType.Text;
                                cmd.ExecuteScalar();

                            }
                        }
                    }

                });
                _logger.LogWarning(identity.GetLogInfo("PUT alokací "));
                return Ok(finalModel);
                //}
            }
            catch (Exception e)
            {
                _logger.LogError(String.Format("{0} {1}", e.Message, e.StackTrace));
                return BadRequest();
            }
        }
    }
}