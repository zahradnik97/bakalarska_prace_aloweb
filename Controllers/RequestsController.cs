﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using zaht02_BP.Helpers;
using zaht02_BP.Models.Dashboard;
using zaht02_BP.Models.Employees;
using zaht02_BP.Models.Requests;

namespace zaht02_BP.Controllers
{
    [Route("api/[controller]")]
    [ApiController, Authorize]
    public class RequestsController : ControllerConfig
    {
        public RequestsController(IOptions<Configuration> configAccesor, ILogger<RequestsController> logger) : base(configAccesor, logger)
        {
        }

        // GET: api/Requests/employeeId
        [HttpGet("{employeeId}")]
        public IActionResult Get(int employeeId)
        {
            try
            {
                List<RequestModel> responseList = new List<RequestModel>();
                using (SqlConnection connection = new SqlConnection(_config.ConnectionString))
                {
                    connection.Open();
                    string sql = String.Format("SELECT * FROM [dbo].Request " +
                        "INNER JOIN [dbo].Employee ON Employee.EmployeeId = Request.EmployeeId " +
                        "WHERE Request.EmployeeId = @EmployeeId", employeeId);
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.Add("@EmployeeId", SqlDbType.Int).Value = employeeId;
                        SqlDataReader dataReader = cmd.ExecuteReader();

                        while (dataReader.Read())
                        {
                            responseList.Add
                                (
                                    new RequestModel()
                                    {
                                        RequestId = (int)dataReader["RequestId"],
                                        Type = dataReader["Type"].ToString(),
                                        ProjectManagerId = (int)dataReader["ProjectManagerId"],
                                        EmployeeId = (int)dataReader["EmployeeId"],
                                        Reason = dataReader["Reason"].ToString(),
                                        DateFrom = dataReader["DateFrom"].ToString(),
                                        DateTo = dataReader["DateTo"].ToString(),
                                        IsApprovedByPM = (bool)dataReader["IsApprovedByPM"],
                                        IsApprovedByHRM = (bool)dataReader["IsApprovedByHRM"],
                                        IsConfirmed = (bool)dataReader["IsConfirmed"],
                                        FirstName = dataReader["FirstName"].ToString(),
                                        LastName = dataReader["LastName"].ToString()
                                    }
                                );
                        }
                    }

                }
                return Ok(responseList);
            }
            catch (Exception e)
            {
                _logger.LogError(String.Format("{0} {1}", e.Message, e.StackTrace));
                return BadRequest();
            }


        }

        [HttpPost]
        public IActionResult Post([FromBody] RequestModel newRequestModel)
        {
            if (Helper.GetIdentityUser(HttpContext.User.Identity as ClaimsIdentity).EmployeeId == newRequestModel.EmployeeId)
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(_config.ConnectionString))
                    {
                        connection.Open();
                        string sql = "INSERT INTO [dbo].Request(Reason,ProjectManagerId,Type,DateFrom,DateTo,EmployeeId,IsApprovedByHRM,IsApprovedByPM,IsConfirmed)" +
                            " VALUES(@Reason,@ProjectManagerId,@Type,@DateFrom,@DateTo,@EmployeeId,@IsApprovedByHRM, @IsApprovedByPM, @IsConfirmed);";
                        using (SqlCommand cmd = new SqlCommand(sql, connection))
                        {
                            cmd.Parameters.Add("@Reason", SqlDbType.Text).Value = newRequestModel.Reason != null ? newRequestModel.Reason : string.Empty;
                            cmd.Parameters.Add("@ProjectManagerId", SqlDbType.Int).Value = newRequestModel.ProjectManagerId;
                            cmd.Parameters.Add("@Type", SqlDbType.VarChar).Value = newRequestModel.Type;
                            cmd.Parameters.Add("@DateFrom", SqlDbType.VarChar).Value = newRequestModel.DateFrom;
                            cmd.Parameters.Add("@DateTo", SqlDbType.VarChar).Value = newRequestModel.DateTo;
                            cmd.Parameters.Add("@EmployeeId", SqlDbType.Int).Value = newRequestModel.EmployeeId;
                            cmd.Parameters.Add("@IsApprovedByPM", SqlDbType.Bit).Value = false;
                            cmd.Parameters.Add("@IsApprovedByHRM", SqlDbType.Bit).Value = false;
                            cmd.Parameters.Add("@IsConfirmed", SqlDbType.Bit).Value = false;

                            cmd.CommandType = CommandType.Text;
                            cmd.ExecuteNonQuery();

                        }
                    }
                    EmployeeModel identity = Helper.GetIdentityUser(HttpContext.User.Identity as ClaimsIdentity);
                    _logger.LogWarning(identity.GetLogInfo("Vytvoření vlastní žádosti "));
                    return Ok(newRequestModel);
                }
                catch (Exception e)
                {
                    _logger.LogError(String.Format("{0} {1}", e.Message, e.StackTrace));
                    return BadRequest();
                }
            }
            else
            {
                return Forbid();
            }

        }

        [HttpDelete("{requestId}/{employeeId}")]
        public IActionResult Delete(int requestId, int employeeId)
        {
            if (Helper.GetIdentityUser(HttpContext.User.Identity as ClaimsIdentity).EmployeeId == employeeId)
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(_config.ConnectionString))
                    {
                        connection.Open();
                        string sql = "DELETE FROM [dbo].Request WHERE RequestId = @RequestId";
                        using (SqlCommand cmd = new SqlCommand(sql, connection))
                        {
                            cmd.Parameters.Add("@RequestId", SqlDbType.Int).Value = requestId;
                            cmd.CommandType = CommandType.Text;
                            cmd.ExecuteNonQuery();
                        }
                    }
                    EmployeeModel identity = Helper.GetIdentityUser(HttpContext.User.Identity as ClaimsIdentity);
                    _logger.LogWarning(identity.GetLogInfo("Odstranění vlastní žádosti "));
                    return Ok();
                }
                catch (Exception e)
                {
                    _logger.LogError(String.Format("{0} {1}", e.Message, e.StackTrace));
                    return BadRequest();
                }
            }
            return Forbid();
        }

        [HttpPut]
        public IActionResult Put([FromBody] RequestModel updatedRequestModel)
        {
            if (Helper.GetIdentityUser(HttpContext.User.Identity as ClaimsIdentity).EmployeeId == updatedRequestModel.EmployeeId)
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(_config.ConnectionString))
                    {
                        connection.Open();
                        string sql = "UPDATE [dbo].Request SET " +
                            "Reason = @Reason, " +
                            "ProjectManagerId = @ProjectManagerId, " +
                            "Type = @Type, " +
                            "DateTo = @DateTo, " +
                            "DateFrom = @DateFrom, " +
                            "IsConfirmed = @IsConfirmed " +
                            "WHERE RequestId = @RequestId";
                        using (SqlCommand cmd = new SqlCommand(sql, connection))
                        {
                            cmd.Parameters.Add("@RequestId", SqlDbType.Int).Value = updatedRequestModel.RequestId;

                            cmd.Parameters.Add("@Reason", SqlDbType.Text).Value = updatedRequestModel.Reason;
                            cmd.Parameters.Add("@ProjectManagerId", SqlDbType.Int).Value = updatedRequestModel.ProjectManagerId;
                            cmd.Parameters.Add("@Type", SqlDbType.VarChar).Value = updatedRequestModel.Type;
                            cmd.Parameters.Add("@DateFrom", SqlDbType.VarChar).Value = updatedRequestModel.DateFrom;
                            cmd.Parameters.Add("@DateTo", SqlDbType.VarChar).Value = updatedRequestModel.DateTo;
                            cmd.Parameters.Add("@IsConfirmed", SqlDbType.Bit).Value = updatedRequestModel.IsConfirmed;
                            cmd.CommandType = CommandType.Text;
                            cmd.ExecuteNonQuery();

                        }

                        if (updatedRequestModel.IsConfirmed && updatedRequestModel.Type != RequestType.HomeOffice.ToString())
                        {
                            var parseDateFrom = updatedRequestModel.DateFrom.Split('.');
                            var parseDateTo = updatedRequestModel.DateTo.Split('.');
                            AllocateConfirmedRequest(
                                new DateTime(int.Parse(parseDateFrom[2]), int.Parse(parseDateFrom[1]), int.Parse(parseDateFrom[0])),
                                new DateTime(int.Parse(parseDateTo[2]), int.Parse(parseDateTo[1]), int.Parse(parseDateTo[0])),
                                updatedRequestModel.EmployeeId);
                        }

                    }
                    EmployeeModel identity = Helper.GetIdentityUser(HttpContext.User.Identity as ClaimsIdentity);
                    _logger.LogWarning(identity.GetLogInfo("Změna vlastní žádosti "));
                    return Ok(updatedRequestModel);
                }
                catch (Exception e)
                {
                    _logger.LogError(String.Format("{0} {1}", e.Message, e.StackTrace));
                    return BadRequest();
                }

            }
            else
            {
                return Forbid();
            }
        }

        [HttpPut("[action]"), Authorize(Roles = "ProjectManager,Administrator")]
        public IActionResult ApproveByPM([FromBody] RequestModel updatedRequestModel)
        {
            var identity = Helper.GetIdentityUser(HttpContext.User.Identity as ClaimsIdentity);
            if (identity.EmployeeId == updatedRequestModel.ProjectManagerId)
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(_config.ConnectionString))
                    {
                        connection.Open();
                        string sql = "UPDATE [dbo].Request SET " +
                            "IsApprovedByPM = @IsApprovedByPM " +
                            "WHERE RequestId = @RequestId";
                        using (SqlCommand cmd = new SqlCommand(sql, connection))
                        {
                            cmd.Parameters.Add("@RequestId", SqlDbType.Int).Value = updatedRequestModel.RequestId;
                            cmd.Parameters.Add("@IsApprovedByPM", SqlDbType.Bit).Value = updatedRequestModel.IsApprovedByPM;
                            cmd.CommandType = CommandType.Text;
                            cmd.ExecuteNonQuery();

                        }
                        try
                        {

                            string getEmployeeSql = "SELECT Email FROM Employee WHERE EmployeeId = @EmployeeId";
                            string email = string.Empty;
                            using (SqlCommand cmd = new SqlCommand(getEmployeeSql, connection))
                            {
                                cmd.Parameters.Add("@EmployeeId", SqlDbType.Int).Value = updatedRequestModel.EmployeeId;
                                email = (string)cmd.ExecuteScalar();
                            }
                            var message = String.Format("Dobrý den, <br> Vaše žádost {0} od {1} do {2} byla {3} projektovým manažerem {4}."
                                , updatedRequestModel.GetRequestTypeEmail()
                                , updatedRequestModel.DateFrom
                                , updatedRequestModel.DateTo
                                , updatedRequestModel.IsApprovedByPM ? "SCHVÁLENA" : "ZAMÍTNUTA"
                                , identity.Name());
                            var subject = String.Format("Vaše žádost byla {0} PM", updatedRequestModel.IsApprovedByPM ? "schválena" : "zamítnuta");
                            Helper.SendEmail(_config, email, message, subject);
                        }
                        catch (Exception e)
                        {
                            _logger.LogError(String.Format("{0} {1}", e.Message, e.StackTrace));
                        }

                    }
                    _logger.LogWarning(identity.GetLogInfo("Potvrzení žádosti od PM "));
                    return Ok(updatedRequestModel);
                }
                catch (Exception e)
                {
                    _logger.LogError(String.Format("{0} {1}", e.Message, e.StackTrace));
                    return BadRequest();
                }
            }
            else
            {
                return Forbid();
            }

        }

        [HttpGet("[action]/{employeeId}"), Authorize(Roles = "ProjectManager,Administrator")]
        public IActionResult GetPMRequests(int employeeId)
        {
            if (Helper.GetIdentityUser(HttpContext.User.Identity as ClaimsIdentity).EmployeeId == employeeId)
            {
                List<RequestModel> responseList = new List<RequestModel>();
                try
                {
                    using (SqlConnection connection = new SqlConnection(_config.ConnectionString))
                    {
                        connection.Open();
                        string sql = String.Format("SELECT * FROM [dbo].Request " +
                        "INNER JOIN [dbo].Employee ON Employee.EmployeeId = Request.EmployeeId " +
                        "WHERE ProjectManagerId = @EmployeeId", employeeId);
                        using (SqlCommand cmd = new SqlCommand(sql, connection))
                        {
                            cmd.Parameters.Add("@EmployeeId", SqlDbType.Int).Value = employeeId;

                            SqlDataReader dataReader = cmd.ExecuteReader();

                            while (dataReader.Read())
                            {
                                responseList.Add
                                    (
                                        new RequestModel()
                                        {
                                            RequestId = (int)dataReader["RequestId"],
                                            Type = dataReader["Type"].ToString(),
                                            ProjectManagerId = (int)dataReader["ProjectManagerId"],
                                            EmployeeId = (int)dataReader["EmployeeId"],
                                            Reason = dataReader["Reason"].ToString(),
                                            DateFrom = dataReader["DateFrom"].ToString(),
                                            DateTo = dataReader["DateTo"].ToString(),
                                            IsApprovedByPM = (bool)dataReader["IsApprovedByPM"],
                                            IsApprovedByHRM = (bool)dataReader["IsApprovedByHRM"],
                                            IsConfirmed = (bool)dataReader["IsConfirmed"],
                                            FirstName = dataReader["FirstName"].ToString(),
                                            LastName = dataReader["LastName"].ToString()
                                        }
                                    );
                            }
                        }

                    }
                }
                catch (Exception e)
                {
                    _logger.LogError(String.Format("{0} {1}", e.Message, e.StackTrace));
                    return BadRequest();
                }

                return Ok(responseList);
            }
            else
            {
                return Forbid();
            }
        }

        [HttpPut("[action]"), Authorize(Roles = "HRManager,Administrator")]
        public IActionResult ApproveByHRM([FromBody] RequestModel updatedRequestModel)
        {
            try
            {
                EmployeeModel identity = Helper.GetIdentityUser(HttpContext.User.Identity as ClaimsIdentity);
                using (SqlConnection connection = new SqlConnection(_config.ConnectionString))
                {
                    connection.Open();
                    string sql = "UPDATE [dbo].Request SET " +
                        "IsApprovedByHRM = @IsApprovedByHRM " +
                        "WHERE RequestId = @RequestId";
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.Add("@RequestId", SqlDbType.Int).Value = updatedRequestModel.RequestId;
                        cmd.Parameters.Add("@IsApprovedByHRM", SqlDbType.Bit).Value = updatedRequestModel.IsApprovedByHRM;
                        cmd.CommandType = CommandType.Text;
                        cmd.ExecuteNonQuery();

                    }

                    try
                    {

                        string getEmployeeSql = "SELECT Email FROM Employee WHERE EmployeeId = @EmployeeId";
                        string email = string.Empty;
                        using (SqlCommand cmd = new SqlCommand(getEmployeeSql, connection))
                        {
                            cmd.Parameters.Add("@EmployeeId", SqlDbType.Int).Value = updatedRequestModel.EmployeeId;
                            email = (string)cmd.ExecuteScalar();
                        }
                        var message = String.Format("Dobrý den, <br> Vaše žádost {0} od {1} do {2} byla {3} personálním manažerem {4}."
                            , updatedRequestModel.GetRequestTypeEmail()
                            , updatedRequestModel.DateFrom
                            , updatedRequestModel.DateTo
                            , updatedRequestModel.IsApprovedByHRM ? "SCHVÁLENA" : "ZAMÍTNUTA"
                            , identity.Name());
                        var subject = String.Format("Vaše žádost byla {0} HR", updatedRequestModel.IsApprovedByHRM ? "schválena" : "zamítnuta");
                        Helper.SendEmail(_config, email, message, subject);

                    }
                    catch (Exception e)
                    {
                        _logger.LogError(String.Format("{0} {1}", e.Message, e.StackTrace));
                    }

                }
                _logger.LogWarning(identity.GetLogInfo("Potvrzení žádosti od HR "));
                return Ok(updatedRequestModel);
            }
            catch (Exception e)
            {
                _logger.LogError(String.Format("{0} {1}", e.Message, e.StackTrace));
                return BadRequest();
            }

        }

        [HttpGet("[action]"), Authorize(Roles = "HRManager,Administrator")]
        public IEnumerable<RequestModel> GetHRRequests()
        {
            List<RequestModel> responseList = new List<RequestModel>();
            try
            {
                using (SqlConnection connection = new SqlConnection(_config.ConnectionString))
                {
                    connection.Open();
                    string sql = String.Format("SELECT * FROM [dbo].Request " +
                        "INNER JOIN [dbo].Employee ON Employee.EmployeeId = Request.EmployeeId ");
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        SqlDataReader dataReader = cmd.ExecuteReader();

                        while (dataReader.Read())
                        {
                            responseList.Add
                                (
                                    new RequestModel()
                                    {
                                        RequestId = (int)dataReader["RequestId"],
                                        Type = dataReader["Type"].ToString(),
                                        ProjectManagerId = (int)dataReader["ProjectManagerId"],
                                        EmployeeId = (int)dataReader["EmployeeId"],
                                        Reason = dataReader["Reason"].ToString(),
                                        DateFrom = dataReader["DateFrom"].ToString(),
                                        DateTo = dataReader["DateTo"].ToString(),
                                        IsApprovedByPM = (bool)dataReader["IsApprovedByPM"],
                                        IsApprovedByHRM = (bool)dataReader["IsApprovedByHRM"],
                                        IsConfirmed = (bool)dataReader["IsConfirmed"],
                                        FirstName = dataReader["FirstName"].ToString(),
                                        LastName = dataReader["LastName"].ToString()

                                    }
                                );
                        }
                    }

                }
            }
            catch (Exception e)
            {
                _logger.LogError(String.Format("{0} {1}", e.Message, e.StackTrace));
            }

            return responseList;
        }

        private void AllocateConfirmedRequest(DateTime startDate, DateTime endDate, int employeeId)
        {
            var dates = new List<DayModel>();

            for (var dt = startDate; dt <= endDate; dt = dt.AddDays(1))
            {
                dates.Add(new DayModel() { Date = dt.Date.ToShortDateString() });
            }

            dates.ForEach((item) =>
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(_config.ConnectionString))
                    {
                        connection.Open();
                        string sql = "SELECT * FROM [dbo].Day WHERE Date = @Date";
                        using (SqlCommand cmd = new SqlCommand(sql, connection))
                        {
                            cmd.Parameters.Add("@Date", SqlDbType.VarChar).Value = item.Date;

                            SqlDataReader dataReader = cmd.ExecuteReader();

                            while (dataReader.Read())
                            {
                                item.DayId = (int)dataReader["DayId"];
                            }

                            dataReader.Close();
                        }

                    }
                }
                catch (Exception e)
                {
                    _logger.LogError(String.Format("{0} {1}", e.Message, e.StackTrace));
                }
                if (item.DayId != 0)
                {
                    try
                    {
                        using (SqlConnection connection = new SqlConnection(_config.ConnectionString))
                        {
                            connection.Open();
                            string sqlDelete = "DELETE FROM [dbo].DayProjectEmployee " +
                                "WHERE EmployeeId = @EmployeeId AND DayId = @DayId";
                            using (SqlCommand cmd = new SqlCommand(sqlDelete, connection))
                            {
                                cmd.Parameters.Add("@EmployeeId", SqlDbType.Int).Value = employeeId;
                                cmd.Parameters.Add("@DayId", SqlDbType.Int).Value = item.DayId;
                                cmd.ExecuteNonQuery();
                            }
                        }

                        var contractType = 0;
                        using (SqlConnection connection = new SqlConnection(_config.ConnectionString))
                        {
                            connection.Open();

                            string sqlInsert = "SELECT ContractType FROM [dbo].[Employee] WHERE EmployeeId = @EmployeeId";
                            using (SqlCommand cmd = new SqlCommand(sqlInsert, connection))
                            {
                                cmd.Parameters.Add("@EmployeeId", SqlDbType.Int).Value = employeeId;
                                contractType = (int)cmd.ExecuteScalar();
                            }
                        }

                        using (SqlConnection connection = new SqlConnection(_config.ConnectionString))
                        {
                            connection.Open();

                            string sqlInsert = "INSERT INTO [dbo].DayProjectEmployee(EmployeeId,ConfirmedRequest,DayId,Allocation,ProjectId)" +
                            " VALUES(@EmployeeId,@ConfirmedRequest,@DayId,@Allocation,@ProjectId);";
                            using (SqlCommand cmd = new SqlCommand(sqlInsert, connection))
                            {
                                cmd.Parameters.Add("@DayId", SqlDbType.Int).Value = item.DayId;
                                cmd.Parameters.Add("@ConfirmedRequest", SqlDbType.Bit).Value = true;
                                cmd.Parameters.Add("@Allocation", SqlDbType.Int).Value = contractType;
                                cmd.Parameters.Add("@EmployeeId", SqlDbType.Int).Value = employeeId;
                                cmd.Parameters.Add("@ProjectId", SqlDbType.Int).Value = _config.HolidayId;

                                cmd.CommandType = CommandType.Text;
                                cmd.ExecuteScalar();

                            }
                        }
                    }
                    catch (Exception e)
                    {
                        _logger.LogError(String.Format("{0} {1}", e.Message, e.StackTrace));
                    }
                }
            });
        }



    }

}