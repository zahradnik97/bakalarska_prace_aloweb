import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { LoginComponent } from './login/login.component';
import { LoginService } from './login/login.service';
import { SharedService } from './shared/shared.service';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { EmployeesComponent } from './employees/employees.component';
import { ProjectsComponent } from './projects/projects.component';
import { RequestsComponent } from './requests/requests.component';
import { ProjectsModule } from './projects/projects.module';
import { RequestsModule } from './requests/requests.module';
import { EmployeesModule } from './employees/employees.module';
import { AbilitiesComponent } from './abilities/abilities.component';
import { AbilitiesModule } from './abilities/abilities.module';
import { ProjectProfileComponent } from './projects/profile/project-profile.component';
import { AppRoutingModule } from './app-routing.module';
import { NOTFOUND } from 'dns';
import { NotFoundComponent } from './not-found.component';
import { ApiService } from './core/api/api.service';
import { CodeTablePipe } from './core/pipes/codetable.pipe';
import { AlertsComponent } from './shared/alerts/alerts.component';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    LoginComponent,
    NotFoundComponent,
    AlertsComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    DashboardModule,
    EmployeesModule,
    ProjectsModule,
    RequestsModule,
    AbilitiesModule,
    AppRoutingModule
  ],
  providers: [LoginService, SharedService, ApiService],
  bootstrap: [AppComponent],
  exports: [AppRoutingModule]
})
export class AppModule { }
