import { Component, OnInit } from '@angular/core';
import { SharedService } from './shared/shared.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class NotFoundComponent implements OnInit {
  title = 'app';
  constructor(private sharedService: SharedService,
    private router: Router
  )
  { }

  ngOnInit() {
  }

}
