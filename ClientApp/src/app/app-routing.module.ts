import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AbilitiesComponent } from './abilities/abilities.component';
import { EmployeesComponent } from './employees/employees.component';
import { ProjectProfileComponent } from './projects/profile/project-profile.component';
import { ProjectsComponent } from './projects/projects.component';
import { RequestsComponent } from './requests/requests.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { EmployeeProfileComponent } from './employees/profile/employee-profile.component';
import { NotFoundComponent } from './not-found.component';
import { AuthGuard } from './auth-guard';

const routes: Routes =
  [
    {
      path: '', component: DashboardComponent, pathMatch: 'full', canActivate: [AuthGuard],
      data: {expectedRoles: ['Administrator', 'HRManager', 'ProjectManager', 'SalesManager', 'Employee']}
    },
    { path: 'login', component: LoginComponent },
    {
      path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard],
      data: {expectedRoles: ['Administrator', 'HRManager', 'ProjectManager', 'SalesManager', 'Employee']}
    },
    {
      path: 'requests', component: RequestsComponent, canActivate: [AuthGuard],
      data: {expectedRoles: ['Administrator', 'HRManager', 'ProjectManager', 'SalesManager', 'Employee']}
    },
    {
      path: 'projects', component: ProjectsComponent, canActivate: [AuthGuard],
      data: {expectedRoles: ['Administrator', 'HRManager', 'ProjectManager', 'SalesManager', 'Employee']}
    },
    {
      path: 'project/:id', component: ProjectProfileComponent, canActivate: [AuthGuard],
      data: {expectedRoles: ['Administrator', 'HRManager', 'ProjectManager', 'SalesManager', 'Employee']}
    },
    {
      path: 'employees', component: EmployeesComponent, canActivate: [AuthGuard],
      data: {expectedRoles: ['Administrator', 'HRManager', 'ProjectManager', 'SalesManager', 'Employee']}
    },
    {
      path: 'employee/:id', component: EmployeeProfileComponent, canActivate: [AuthGuard],
      data: {expectedRoles: ['Administrator', 'HRManager', 'ProjectManager', 'SalesManager', 'Employee']}
    },
    {
      path: 'abilities', component: AbilitiesComponent, canActivate: [AuthGuard],
      data: { expectedRoles: ['Administrator','HRManager', 'ProjectManager', 'SalesManager']}
    },
    {
      path: '**', component: DashboardComponent, canActivate: [AuthGuard],
      data: { expectedRoles: ['Administrator', 'HRManager', 'ProjectManager', 'SalesManager', 'Employee'] }
    }
  ];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})

export class AppRoutingModule {}
