"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Employee = /** @class */ (function () {
    function Employee() {
        this.username = "";
        this.firstName = "";
        this.lastName = "";
        this.contractType = 0;
        this.position = "";
        this.email = "@";
    }
    return Employee;
}());
exports.Employee = Employee;
var FilterEmployeeModel = /** @class */ (function () {
    function FilterEmployeeModel() {
        this.firstName = "";
        this.lastName = "";
        this.position = "";
    }
    return FilterEmployeeModel;
}());
exports.FilterEmployeeModel = FilterEmployeeModel;
//# sourceMappingURL=employee.model.js.map
