import { IAbility } from "../../shared/models/ability.model";

export interface IEmployee {
  employeeId?: number;
  username?: string;
  firstName?: string;
  lastName?: string;
  abilities?: IAbility[];
  contractType?: number;
  position?: string;
  roles?: string[];
  email?: string;
}

export interface IEmployeeAuth {
  EmployeeId?: number;
  FirstName?: string;
  LastName?: string;
  Roles?: string[];
  Email?: string;
}

export class Employee implements IEmployee {
  username= "";
  firstName= "";
  lastName= "";
  contractType= 0;
  position = "";
  email = "@";
}

export class FilterEmployeeModel {
  firstName = "";
  lastName = "";
  position = "";
  abilities?: IAbility[]
}
