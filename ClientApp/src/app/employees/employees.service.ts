import { Injectable, Inject } from '@angular/core';
import { ApiConfig } from '../api.config';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../core/api/api.service';
import { IEmployee } from './models/employee.model';
import { Router } from '@angular/router';
import { RoleType } from '../shared/models/role.model';

@Injectable()
export class EmployeesService extends ApiService {
  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string, router: Router) {
    super(http, baseUrl, router);
  }

  getEmployees$(): Observable<IEmployee[]> {
    return super.get(ApiConfig.endpoints.employees.url);
  }

  getEmployee$(employeeId: number): Observable<IEmployee> {
    return super.getById(ApiConfig.endpoints.employees.url, employeeId);
  }

  updateEmployee$(employee: IEmployee): Observable<any> {
    return super.put(ApiConfig.endpoints.employees.url, employee);
  }

  addEmployee$(employee: IEmployee): Observable<any> {
    return super.post(ApiConfig.endpoints.employees.url, employee);
  }

  deleteEmployee$(employeeId: number): Observable<IEmployee> {
    return super.deleteById(ApiConfig.endpoints.employees.url, employeeId);
  }

  getRoleType(role: string): string {
    switch (role) {
      case RoleType.Administrator:
        return "Administrátor";
      case RoleType.Employee:
        return "Zaměstnanec";
      case RoleType.HRManager:
        return "Personální manažer";
      case RoleType.ProjectManager:
        return "Projektový manažer";
      case RoleType.SalesManager:
        return "Sales Manažer";
    }
  }

}
