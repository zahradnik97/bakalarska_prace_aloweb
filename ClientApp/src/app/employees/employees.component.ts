import { Component, OnInit, ElementRef, HostListener } from '@angular/core';
import { EmployeesService } from './employees.service';
import { IEmployee, Employee } from './models/employee.model';
import { IAbilityModel } from '../abilities/models/ability.model';
import { SharedService } from '../shared/shared.service';
import { CodeTableType, ICodeTableItem } from '../shared/models/code-table.model';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';

declare var swal: any;

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {

  constructor(private employeesService: EmployeesService,
    private sharedService: SharedService,
    private formBuilder: FormBuilder) { }

  abilitiesCodeTable: ICodeTableItem[] = [];
  employeesCodeTable: ICodeTableItem[] = [];

  employeesList: IEmployee[] = [];

  addNewForm: FormGroup;

  employeeFilterInput: string = "";
  newAbilities: IAbilityModel[] = [];
  newRoles: string[] = ["Employee"];

  header: HTMLElement;
  stickyPosition: number;
  @HostListener('window:scroll', ['$event'])
  scrollHandler(event) {
    if (window.pageYOffset > this.stickyPosition) {
      this.header.classList.add("sticky");
    } else {
      this.header.classList.remove("sticky");
    }
  }

  ngOnInit() {
    this.header = document.getElementById("stickyHeader");
    this.stickyPosition = this.header.offsetTop;
    this.getAllEmployees();
    this.getCodeTables();
    this.createForm();
  }

  createForm() {
    this.addNewForm = this.formBuilder.group({
      firstName: [null, Validators.required],
      lastName: [null, Validators.required],
      contractType: [null, Validators.compose([Validators.required, Validators.max(8), Validators.min(1)])],
      position: [null, Validators.required],
      email: [null, Validators.compose([Validators.required, Validators.email])]
    });
  }

  getAllEmployees() {
    this.sharedService.showLoader();
      this.employeesService.getEmployees$().subscribe((res) => {
        this.employeesList = res;
        this.sharedService.disableLoader();
      }, (e) => {
          this.sharedService.disableLoader();
      }
      );
    }

  getCodeTables() {
    this.sharedService.getCodeTable$(CodeTableType.abilities).subscribe((res) => {
      this.abilitiesCodeTable = res;
    }, (e) => {
    });

    this.sharedService.getCodeTable$(CodeTableType.employees).subscribe((res) => {
      this.employeesCodeTable = res;
    }, (e) => {
    });
  }

  addAbility(value) {
    let duplicated = false;
    this.newAbilities.forEach((item) => {
      if (item.name == value)
        duplicated = true;
    });
    if (!duplicated) {
      let abilityToAdd = this.abilitiesCodeTable.find((item) => item.name === value);
      this.newAbilities.push({ abilityId: abilityToAdd.id, name: abilityToAdd.name });
    }
  }

  deleteAbility(index) {
    this.newAbilities.splice(index, 1);
  }

  addNewEmployee() {
    if (!this.addNewForm.valid) {
      this.sharedService.markFormGroupTouched(this.addNewForm);
      return;
    }

    const newEmployee: IEmployee = {
      ...this.addNewForm.value,
      abilities: this.newAbilities,
      contractType: +this.addNewForm.value.contractType,
      roles: this.newRoles
    };
    this.sharedService.showLoader();
    this.employeesService.addEmployee$(newEmployee).subscribe((res) => {
      if (res.error === "EMAIL_EXISTS") {
        swal({
          icon: 'error',
          title: `Nový zaměstnanec`,
          text: `Zaměstnanec s emailem ${newEmployee.email} již existuje.`,
        });
      }
      else {
        this.newAbilities = [];
        this.newRoles = ["Employee"];
        this.getAllEmployees();
        this.addNewForm.reset();
        this.addNewForm.markAsUntouched();
        (document.getElementsByClassName('dismiss-modal')[0] as any).click();
        swal({
          icon: 'success',
          title: `Nový zaměstnanec`,
          text: `Zaměstnanec byl úspěšně vytvořen.`,
          timer: 1300
        });
      }
      this.sharedService.disableLoader();
      console.log(res);
    }, (e) => {
        this.sharedService.disableLoader();
    });
  }

  addRole(role) {
    if (!this.newRoles.includes(role)) {
      this.newRoles.push(role);
    }
  }

  deleteRole(role) {
    this.newRoles.splice(this.newRoles.indexOf(role), 1);
  }

}
