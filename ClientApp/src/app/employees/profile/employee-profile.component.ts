import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { EmployeesService } from '../employees.service';
import { IEmployee, Employee } from '../models/employee.model';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { SharedService } from '../../shared/shared.service';
import { CodeTableType, ICodeTableItem } from '../../shared/models/code-table.model';
import { IAbility } from '../../shared/models/ability.model';
import { LoginService } from '../../login/login.service';

declare var swal: any;

@Component({
  selector: 'app-employee-profile',
  templateUrl: './employee-profile.component.html',
  styleUrls: ['./employee-profile.component.css']
})
export class EmployeeProfileComponent implements OnInit {

  employee: IEmployee = {};
  form: FormGroup;
  abilitiesCodeTable: ICodeTableItem[] = [];
  newAbilities: IAbility[] = [];

  constructor(private activatedRoute: ActivatedRoute,
    private employeesService: EmployeesService,
    private router: Router,
    private formBuilder: FormBuilder,
    private sharedService: SharedService,
    private loginService: LoginService) { }

  ngOnInit() {
    this.buildForm();
    this.getCodeTables();
    let employeeId = parseInt(this.activatedRoute.snapshot.paramMap.get("id"));
    if (employeeId) {
      this.sharedService.showLoader();
      this.employeesService.getEmployee$(employeeId).subscribe((res) => {
       
          this.employee = res;
          this.newAbilities = this.employee.abilities;
          this.sharedService.patchForm(this.form, res);
        this.sharedService.disableLoader();
      }, (e) => {
          this.sharedService.disableLoader();
          swal({
            icon: 'error',
            title: `Profil zaměstnance`,
            text: 'Zaměstnanec neexistuje, budete přesměrováni na výpis zaměstnanců.',
          });
          this.router.navigate(['/employees']);
      });
    }
    else {
      this.router.navigate(['/employees']);
    }
  }

  getCodeTables() {
    this.sharedService.getCodeTable$(CodeTableType.abilities).subscribe((res) => {
      this.abilitiesCodeTable = res;
    }, (e) => {
    });
  }

  buildForm() {
      this.form = this.formBuilder.group({
        firstName: [null, Validators.required],
        lastName: [null, Validators.required],
        contractType: [null, Validators.compose([Validators.required, Validators.max(8), Validators.min(1)])],
        position: [null, Validators.required],
        email: [null, Validators.compose([Validators.required, Validators.email])]
      });
  }

  editEmployee() {
    if (!this.form.valid) {
      this.sharedService.markFormGroupTouched(this.form);
      return;
    }

    const updateModel: IEmployee = {
      ...this.employee,
      ...this.form.value
    };
    this.sharedService.showLoader();
    this.employeesService.updateEmployee$(updateModel).subscribe((res) => {
      if (res.error === "EMAIL_EXISTS") {
        swal({
          icon: 'error',
          title: `Úprava zaměstnance`,
          text: `Zaměstnanec s emailem ${updateModel.email} již existuje.`,
        });
      }
      else {
        console.log(res);
        this.employee = res;
        (document.getElementsByClassName('dismiss-modal')[0] as any).click();
        swal({
          icon: 'success',
          title: `Úprava zaměstnance`,
          text: `Zaměstnanec byl úspěšně upraven.`,
          timer: 1300
        });
      }
      this.sharedService.disableLoader();
    }, (e) => {
        this.sharedService.disableLoader();
    });
  }

  addAbility(ability) {
    let include: boolean = false;
    this.employee.abilities.forEach((item) => {
      if (item.name === ability) {
        include = true;
      }
    });
    if (!include) {
      let newAbility = this.abilitiesCodeTable.find((item) => item.name === ability);
      this.employee.abilities.push({ abilityId: newAbility.id, name: newAbility.name });
    }
    else {
      swal({
        icon: 'error',
        title: `Přidání schopnosti`,
        text: `Tato schopnost již přiřazena je.`,
      });
    }
  }

  deleteAbility(index) {
    this.employee.abilities.splice(index,1);
  }

  addRole(role) {
    if (!this.employee.roles.includes(role)) {
      this.employee.roles.push(role);
    }
  }

  deleteRole(role) {
    this.employee.roles.splice(this.employee.roles.indexOf(role), 1);
  }

  deleteEmployee() {
    let employee = this.employee;

    swal({
      title: 'Odstranění zaměstnance',
      text: `Opravdu chcete odstranit zaměstnance ${employee.firstName} ${employee.lastName}?`,
      icon: 'warning',
      dangerMode: true,
      buttons: ["Zrušit", `Odstranit`],
    }).then(userInput => {
      if (userInput) {
        this.employeesService.deleteEmployee$(this.employee.employeeId).subscribe((res) => {
          this.router.navigate(['/employees']);
          console.log(res);
          swal({
            icon: 'success',
            title: `Odstranění zaměstnance`,
            text: `Zaměstnanec byl odstraněn`,
            timer: 1300
          });
        }, (e) => {
        });

      }
    });

  }

  resetPassword() {
    let employee = this.employee;
    swal({
      title: 'Reset hesla',
      text: `Opravdu chcete resetovat heslo zaměstnance ${employee.firstName} ${employee.lastName}?`,
      icon: 'warning',
      dangerMode: true,
      buttons: ["Zrušit", `Resetovat heslo`],
    }).then(userInput => {
      if (userInput) {
        this.sharedService.showLoader();
        this.loginService.$resetPassword(this.employee.employeeId).subscribe((res) => {
          this.sharedService.disableLoader();
          swal({
            icon: 'success',
            title: `Reset hesla`,
            text: `Heslo bylo resetováno`,
            timer: 1300
          });
        }, (e) => {
          this.sharedService.disableLoader();
        });
      }
    });


  
  }
}
