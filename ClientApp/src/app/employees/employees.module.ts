import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeesComponent } from './employees.component';
import { EmployeesService } from './employees.service';
import { FilterModule } from '../shared/utils/filter.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedService } from '../shared/shared.service';
import { EmployeeProfileComponent } from './profile/employee-profile.component';
import { AppRoutingModule } from '../app-routing.module';
import { CodeTablePipe } from '../core/pipes/codetable.pipe';
import { LoginService } from '../login/login.service';

@NgModule({
  imports: [
    CommonModule,
    FilterModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  declarations: [EmployeesComponent, EmployeeProfileComponent],
  providers: [EmployeesService, SharedService, LoginService]
})
export class EmployeesModule { }
