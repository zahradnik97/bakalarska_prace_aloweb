import { Component, AfterViewInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { SharedService } from '../shared/shared.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { LoginService } from '../login/login.service';
import { IChangePasswordModel } from '../shared/models/change-password.model';
import { CustomValidators } from '../shared/validators/custom-validators';

declare var swal: any;

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent implements AfterViewInit {

  form: FormGroup;
  userName: string = "";
  constructor(private router: Router,
    private sharedService: SharedService,
    private loginService: LoginService,
    private formBuilder: FormBuilder)
  { }

  ngOnInit() {
    this.buildForm();
    this.userName = this.sharedService.user.LastName + ' ' + this.sharedService.user.FirstName;
  }

  openNav = false;

  buildForm() {
    this.form = this.formBuilder.group({
      oldPassword: ['', Validators.required],
      newPassword: ['', Validators.compose([
        Validators.required,
        CustomValidators.patternValidator(/\d/, { hasNumber: true }),
        CustomValidators.patternValidator(/[A-Z]/, { hasCapitalCase: true }),
        CustomValidators.patternValidator(/[a-z]/, { hasSmallCase: true }),
        Validators.minLength(8)]
      )],
      newPasswordAgain: ['',Validators.required]
    }, {
        validator: CustomValidators.passwordMatchValidator
    });
  }

  changePassword() {
    console.log(this.form);
    if (!this.form.valid) {
      this.sharedService.markFormGroupTouched(this.form);
      return;
    }

    const putModel: IChangePasswordModel = {
      newPassword: this.form.value.newPassword,
      employeeId: this.sharedService.user.EmployeeId,
      oldPassword: this.form.value.oldPassword
    }

    this.loginService.$changePassword(putModel).subscribe((res) => {
      if (res.message === "wrong_password") {
        swal({
          title: 'Změna hesla',
          text: "Zadané aktuální heslo je nesprávné.",
          icon: 'error',
        });
      }
      else {
        setTimeout(() => {
          this.sharedService.passwordChanged = true;
        }, 0);
        this.form.reset();
        (document.getElementsByClassName('dismiss-modal-password')[0] as any).click();
        swal({
          title: 'Změna hesla',
          text: "Heslo bylo úspěšně změněno.",
          icon: 'success',
          timer: 1300
        });
      }
    }, (e) => {
    });
  }

  isExpanded = false;

  collapse() {
    this.isExpanded = false;
  }

  toggle() {
    this.isExpanded = !this.isExpanded;
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      if (!this.sharedService.passwordChanged) {
        swal({
          title: 'Změna hesla',
          text: "Změňte si prosím heslo, aktuální bylo vygenerováno systémem.",
          icon: 'warning',
          dangerMode: true,
          buttons: ["Později", "Změnit heslo"],
        }).then( userInput => {
          if (userInput) {
            (document.getElementById("passwordChangeModal") as any).click();
           
          }
        });
      }
    }, 900);
  }
}
