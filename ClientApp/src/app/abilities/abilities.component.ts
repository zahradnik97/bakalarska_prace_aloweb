import { Component, OnInit, HostListener } from '@angular/core';
import { IAbilityModel } from './models/ability.model';
import { AbilitiesService } from './abilities.service';
import { SharedService } from '../shared/shared.service';

declare var swal: any;

@Component({
  selector: 'app-abilities',
  templateUrl: './abilities.component.html',
  styleUrls: ['./abilities.component.css']
})
export class AbilitiesComponent implements OnInit {

  abilitiesList: IAbilityModel[] = [];
  abilitiesFilterInput: string = "";
  updateAbilityObject: IAbilityModel = {};
  header: HTMLElement;
  stickyPosition: number;
  @HostListener('window:scroll', ['$event'])
  scrollHandler(event) {
    if (window.pageYOffset > this.stickyPosition) {
      this.header.classList.add("sticky");
    } else {
      this.header.classList.remove("sticky");
    }
  }

  constructor(private abilitiesService: AbilitiesService,
    private sharedService: SharedService) { }

  ngOnInit() {
    this.header = document.getElementById("stickyHeader");
    this.stickyPosition = this.header.offsetTop;
    this.getAllAbilities();
  }

  getAllAbilities() {
    this.sharedService.showLoader();
    this.abilitiesService.getAbilitiesList$().subscribe((res) => {
      this.abilitiesList = [];
      Object.assign(this.abilitiesList, res);
      this.sharedService.disableLoader();
    }, (e) => {
        this.sharedService.disableLoader();
    });
  }

  validateAbility(value) {
    if (value.length === 0) {
      return false;
    }
    var isDuplicated: boolean = false;
    this.abilitiesList.forEach((item) => {
      if (item.name === value) {
        isDuplicated = true;
        swal({
          icon: 'error',
          title: `Nová schopnost`,
          text: 'Tato schopnost je již přidána.',
        });
      }
    });
    return !isDuplicated;
  }

  addNewAbility(abilityValue) {
    let value = abilityValue.value.trim();
   

    if (this.validateAbility(value)) {

      let newAbility: IAbilityModel = {
        name: value
      };
      this.abilitiesService.addNewAbility$(newAbility).subscribe((res) => {
        console.log(res);
        this.getAllAbilities();
        abilityValue.value = null;
        (document.getElementsByClassName('dismiss-modal-new')[0] as any).click();
        swal({
          icon: 'success',
          title: `Nová schopnost`,
          text: `Schopnost byla úspěšně přidána.`,
          timer: 1300
        });
      }, (e) => {
      });

    }

  }

  deleteAbility(ability: IAbilityModel) {
    let abilityName = ability.name;

    swal({
      title: 'Odstranění schopnosti',
      text: `Opravdu chcete odstranit schopnost ${abilityName}?`,
      icon: 'warning',
      dangerMode: true,
      buttons: ["Zrušit", `Odstranit`],
    }).then(userInput => {
      if (userInput) {
        this.abilitiesService.deleteAbility$(ability.abilityId).subscribe((res) => {
          console.log(res);
          this.getAllAbilities();
          swal({
            icon: 'success',
            title: `Odstranění schopnosti`,
            text: `Schopnost byla úspěšně odstraněna`,
            timer: 1300
          });
        }, (e) => {
        });

      }
    });
 
  }

  updateAbility(name: string) {
    this.updateAbilityObject.name = name.trim();
    if (this.validateAbility(this.updateAbilityObject.name)) {

    this.abilitiesService.updateAbility$(this.updateAbilityObject).subscribe((res) => {
      console.log(res);
      this.getAllAbilities();
      (document.getElementsByClassName('dismiss-modal-edit')[0] as any).click();
      swal({
        icon: 'success',
        title: `Úprava schopnosti`,
        text: `Schopnost byla úspěšně upravena`,
        timer: 1300
      });
    }, (e) => {
    });
    }
  }

  setUpdateAbility(ability: IAbilityModel) {
    this.updateAbilityObject = ability;
  }
  

}
