export interface IAbilityModel {
  abilityId?: number;
  name?: string;
}

export class FilterAbilityModel {
  name?: string;
}
