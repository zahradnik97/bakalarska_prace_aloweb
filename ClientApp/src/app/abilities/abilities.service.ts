import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../core/api/api.service';
import { Observable } from 'rxjs/Observable';
import { IAbilityModel } from './models/ability.model';
import { ApiConfig } from '../api.config';
import { Router } from '@angular/router';

@Injectable()
export class AbilitiesService extends ApiService {
  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string, router: Router) {
    super(http, baseUrl, router);
  }
  getAbilitiesList$(): Observable<IAbilityModel[]> {
    return super.get(ApiConfig.endpoints.abilities.url);
  }

  addNewAbility$(model: IAbilityModel): Observable<any> {
    return super.post(ApiConfig.endpoints.abilities.url, model);
  }

  deleteAbility$(abilityId: number): Observable<any> {
    return super.deleteById(ApiConfig.endpoints.abilities.url, abilityId);
  }

  updateAbility$(ability: IAbilityModel): Observable<IAbilityModel> {
    return super.put(ApiConfig.endpoints.abilities.url, ability);
  }
}
