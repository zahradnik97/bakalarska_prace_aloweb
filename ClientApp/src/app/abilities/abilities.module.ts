import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AbilitiesComponent } from './abilities.component';
import { AbilitiesService } from './abilities.service';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { FilterModule } from '../shared/utils/filter.module';

@NgModule({
  imports: [
    CommonModule,
    FilterModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [AbilitiesComponent],
  providers: [AbilitiesService]
})
export class AbilitiesModule { }
