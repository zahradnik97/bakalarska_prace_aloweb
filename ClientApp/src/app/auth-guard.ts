import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot } from '@angular/router';
import { SharedService } from './shared/shared.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private sharedService: SharedService, private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot) {
    if (!this.sharedService.isUserLogged()) {
      this.router.navigate(['/login']);
      return false;
    }

    this.sharedService.authorize().subscribe((res) => {
        
    }, (e) => {
        this.router.navigate(['/login']);
        this.sharedService.logOut();
        return false; 
    });
    if (this.sharedService.user.Roles) {
      if (this.sharedService.user.Roles.length === 0) {
        this.router.navigate(['/login']);
        this.sharedService.logOut();
        return false;
      }

      const expectedRoles = route.data.expectedRoles;
      for (let role of this.sharedService.user.Roles) {
        if (expectedRoles.includes(role)) {
          return true;
        }
      }
      this.router.navigate(['/dashboard']);
      return false;
    }
  }
}
