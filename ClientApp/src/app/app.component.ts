import { Component, OnInit } from '@angular/core';
import { SharedService } from './shared/shared.service';
import { Router } from '@angular/router';
import { LoginService } from './login/login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';
  constructor(private sharedService: SharedService,
    private router: Router,
    private loginService: LoginService
  )
  { }

  ngOnInit() {
    if (localStorage.getItem("AccessToken")) {
      //let jsonObject = JSON.parse(atob(localStorage.getItem("AccessToken").split('.')[1]));
      //this.sharedService.user = JSON.parse(jsonObject[Object.keys(jsonObject)[0]]);
      //console.log("USER ************* " + JSON.stringify(this.sharedService.user));
      this.sharedService.authorize().subscribe((res) => {
        this.sharedService.user = res;
      }, (e) => {
      });
    }
  }

}
