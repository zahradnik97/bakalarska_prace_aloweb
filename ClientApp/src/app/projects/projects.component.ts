import { Component, OnInit, HostListener } from '@angular/core';
import { IProject } from '../shared/models/project.model';
import { ProjectsService } from './projects.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { SharedService } from '../shared/shared.service';
import { ICodeTableItem, CodeTableType } from '../shared/models/code-table.model';
import { forkJoin } from 'rxjs/observable/forkJoin';

declare var swal: any;

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit {

  projectManagersCodeTable: ICodeTableItem[] = [];
  abilitiesCodeTable: ICodeTableItem[] = [];

  projectsList: IProject[] = [];
  projectToEdit: IProject = {};
  projectFilterInput: string = "";
  newProjectForm: FormGroup;
  updateProjectForm: FormGroup;

  header: HTMLElement;
  stickyPosition: number;
  @HostListener('window:scroll', ['$event'])
  scrollHandler(event) {
    if (window.pageYOffset > this.stickyPosition) {
      this.header.classList.add("sticky");
    } else {
      this.header.classList.remove("sticky");
    }
  }
 
  constructor(private projectsService: ProjectsService,
    private formBuilder: FormBuilder,
    private sharedService: SharedService) { }

  ngOnInit() {
    this.header = document.getElementById("stickyHeader");
    this.stickyPosition = this.header.offsetTop;
    this.getAllProjects();
    this.createForm();
    this.getCodeTables();
  }

  getCodeTables() {
    forkJoin(
      this.sharedService.getProjectManagerCodeTable(),
      this.sharedService.getCodeTable$(CodeTableType.abilities))
      .subscribe(([projectManagers, abilities]) => {
        this.projectManagersCodeTable = projectManagers;
        this.abilitiesCodeTable = abilities;
      });
  }

  getAllProjects() {
    this.sharedService.showLoader();
    this.projectsService.getProjects$().subscribe((res) => {
      this.projectsList = res;
      this.sharedService.disableLoader();
    }, (e) => {
        this.sharedService.disableLoader();
    });
  }

  createForm() {
    this.newProjectForm = this.formBuilder.group(
      {
        name: [null, Validators.required],
        projectManagerId: [null, Validators.required],
        isBillable: [false],
        color: [null, Validators.required],
        client: [null, Validators.required],
        note: [null]
      }
    );
  }

  addNewProject() {
    console.log(this.newProjectForm);
    if (!this.newProjectForm.valid) {
      this.sharedService.markFormGroupTouched(this.newProjectForm);
      return;
    }

    const newProjectItem: IProject = { ...this.newProjectForm.value, isBillable: !this.newProjectForm.value.isBillable  };
    this.sharedService.showLoader();
    this.projectsService.addNewProject$(newProjectItem).subscribe((res) => {
      if (res.error === "NAME_EXISTS") {
        swal({
          icon: 'error',
          title: `Nový projekt`,
          text: `Projekt s názvem ${newProjectItem.name} již existuje`,
        });
      }
      else {
        this.getAllProjects();
        this.newProjectForm.reset();
        (document.getElementsByClassName('dismiss-modal')[0] as any).click();
        swal({
          icon: 'success',
          title: `Nový projekt`,
          text: `Projekt byl úspěšně vytvořen.`,
          timer: 1300
        });
      }
        this.sharedService.disableLoader();
      }, (e) => {
          this.sharedService.disableLoader();
          
      });


  }

}
