import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProjectsService } from '../projects.service';
import { IProject, Project } from '../../shared/models/project.model';
import { SharedService } from '../../shared/shared.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ICodeTableItem, CodeTableType } from '../../shared/models/code-table.model';

declare var swal: any;

@Component({
  selector: 'app-project-profile',
  templateUrl: './project-profile.component.html',
  styleUrls: ['./project-profile.component.css']
})
export class ProjectProfileComponent implements OnInit {

  project: IProject;
  form: FormGroup;
  projectManagersCodeTable: ICodeTableItem[] = [];

  constructor(private activatedRoute: ActivatedRoute,
    private projectsService: ProjectsService,
    private formBuilder: FormBuilder,
    private router: Router,
    private sharedService: SharedService) { }

  ngOnInit() {
    this.getCodeTables();
    this.form = this.formBuilder.group(
      {
        name: [null, Validators.required],
        projectManagerId: [null, Validators.required],
        isBillable: [false],
        color: [null, Validators.required],
        client: [null, Validators.required],
        note: [null]
      }
    );
    this.getProject();
  }

  getProject() {
    let projectId = parseInt(this.activatedRoute.snapshot.paramMap.get("id"));
    if (projectId) {
      this.sharedService.showLoader();
      this.projectsService.getProject$(projectId).subscribe((res) => {
        this.project = res;
        this.sharedService.patchForm(this.form, res);
        this.form.get('isBillable').setValue(!res.isBillable);
        console.log(this.form);
        this.sharedService.disableLoader();
      }, (e) => {
          this.sharedService.disableLoader();
          swal({
            icon: 'error',
            title: `Profil projektu`,
            text: 'Projekt neexistuje, budete přesměrováni na výpis projektů.',
          });
        this.router.navigate(['/projects']);
      });
    }
    else {
      this.router.navigate(['/projects']);
    }
  }

  getCodeTables() {
    this.sharedService.getProjectManagerCodeTable().subscribe((res) => {
      this.projectManagersCodeTable = res;
    }, (e) => {
    });
  }

  updateProject() {
    if (!this.form.valid) {
      this.sharedService.markFormGroupTouched(this.form);
      return;
    }

    const updateModel: IProject = {
      ...this.project,
      ...this.form.value
    };
    this.sharedService.showLoader();
    this.projectsService.updateProject$(updateModel).subscribe((res) => {
      if (res.error === "NAME_EXISTS") {
        swal({
          icon: 'error',
          title: `Úprava projektu`,
          text: `Projekt s názvem ${updateModel.name} již existuje`,
        });
      }
      else {
        this.getProject();
        (document.getElementsByClassName('dismiss-modal')[0] as any).click();
        swal({
          icon: 'success',
          title: `Úprava projektu`,
          text: `Projekt byl úspěšně upraven.`,
          timer: 1300
        });
        console.log(res);
      }
      this.sharedService.disableLoader();
    }, (e) => {
      this.sharedService.disableLoader();
    });
  }

  deleteProject() {
    let project = this.project;

    swal({
      title: 'Odstranění projektu',
      text: `Opravdu chcete odstranit projekt ${project.name}?`,
      icon: 'warning',
      dangerMode: true,
      buttons: ["Zrušit", `Odstranit`],
    }).then(userInput => {
      if (userInput) {
        this.projectsService.deleteProject$(this.project.projectId).subscribe((res) => {
          console.log(res);
          swal({
            icon: 'success',
            title: `Odstranění projektu`,
            text: `Projekt byl odstraněn`,
            timer: 1300
          });
          this.router.navigate(['/projects']);
        }, (e) => {
        });

      }
    });

  }

}
