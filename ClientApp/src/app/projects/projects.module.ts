import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectsComponent } from './projects.component';
import { ProjectsService } from './projects.service';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { FilterModule } from '../shared/utils/filter.module';
import { ProjectProfileComponent } from './profile/project-profile.component';
import { AppRoutingModule } from '../app-routing.module';
import { SharedService } from '../shared/shared.service';
import { ColorPickerModule } from 'ngx-color-picker';
  
@NgModule({
  imports: [
    CommonModule,
    FilterModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    ColorPickerModule
  ],
  declarations: [ProjectsComponent, ProjectProfileComponent],
  providers: [ProjectsService, SharedService]
})
export class ProjectsModule { }
