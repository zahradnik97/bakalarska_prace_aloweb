import { Injectable, Inject } from '@angular/core';
import { ApiConfig } from '../api.config';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../core/api/api.service';
import { IProject } from '../shared/models/project.model';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';

@Injectable()
export class ProjectsService extends ApiService {
  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string, router: Router) {
    super(http, baseUrl, router);
  }

  getProjects$(): Observable<IProject[]> {
    return super.get(ApiConfig.endpoints.projects.url);
  }

  getProject$(projectId:number): Observable<IProject> {
    return super.getById(ApiConfig.endpoints.projects.url, projectId).pipe(
      map((item) => {
        item.projectManagerId = item.projectManager.employeeId;
        return item;
      })
    );
  }

  addNewProject$(project: IProject): Observable<any> {
    return super.post(ApiConfig.endpoints.projects.url, project);
  }

  deleteProject$(projectId: number): Observable<any> {
    return super.deleteById(ApiConfig.endpoints.projects.url, projectId);
  }

  updateProject$(project: IProject): Observable<any> {
    return super.put(ApiConfig.endpoints.projects.url, project);
  }
}
