import { Pipe, PipeTransform } from '@angular/core';
import { SharedService } from '../../shared/shared.service';
import { CodeTableType, ICodeTableItem } from '../../shared/models/code-table.model';

@Pipe({ name: 'codeTablePipe' })
export class CodeTablePipe implements PipeTransform {
  employeesCodeTable: ICodeTableItem[] = [];
  projectsCodeTable: ICodeTableItem[] = [];
  constructor(private sharedService: SharedService) {
    this.sharedService.getCodeTable$(CodeTableType.employees).subscribe((res) => {
      this.employeesCodeTable = res;
    }, (e) => {
    });
    this.sharedService.getCodeTable$(CodeTableType.projects).subscribe((res) => {
      this.projectsCodeTable = res;
    }, (e) => {
    })
  }
  transform(value, type: string) {
    let returnValue = "";
    switch (type) {
      case CodeTableType.projects:
        this.projectsCodeTable
          .forEach((item) => {
            if (item.id === value)
              returnValue = item.name;
          });
        break;
      case CodeTableType.employees:
        this.employeesCodeTable
          .forEach((item) => {
            if (item.id === value)
              returnValue = item.name;
          });
        break;
    }
    
    return returnValue;
  }

}
