import { Observable } from "rxjs/Observable";
import { HttpErrorResponse, HttpClient } from "@angular/common/http";
import { Inject, Injectable } from "@angular/core";
import { Local } from "protractor/built/driverProviders";
import { ActivatedRoute, Router } from "@angular/router";
import { catchError, map } from "rxjs/operators";

@Injectable()
export class ApiService {

  baseApiUrl: string
  options: any;

  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string, protected router: Router) {
    this.init();
    this.baseApiUrl = baseUrl + "api";
  }

  private init() {
    this.options = {
      observe: 'body'
    };
  }

  delete<T>(path: string, body: any): Observable<T> {
    return this.http.post<T>(this.baseApiUrl + path, body, this.getHeaders())
      .pipe(catchError(this.handleError));
  }

  deleteById<T>(path: string, id: any): Observable<T> {
    return this.http.delete<T>(this.baseApiUrl + path + "/" + id, this.getHeaders())
      .pipe(catchError(this.handleError));
  }

  put<T>(path: string, body: any): Observable<T> {
    return this.http.put<T>(this.baseApiUrl + path, body, this.getHeaders())
      .pipe(catchError(this.handleError));
  }

  post<T>(path: string, body: any): Observable<T> {
    return this.http.post<T>(this.baseApiUrl + path, body, this.getHeaders())
      .pipe(catchError(this.handleError));
  }

  get<T>(path: string): Observable<T> {
    return this.http.get<T>(this.baseApiUrl + path, this.getHeaders())
      .pipe(catchError(this.handleError));
  }

  getById<T>(path: string, id: any): Observable<any> {
    const clearPath = path.replace(/\/$/, '');

    return this.http.get<T>(this.baseApiUrl + clearPath + '/' + id, this.getHeaders())
      .pipe(catchError(this.handleError));
  }

  private getHeaders() {
    return {
      headers: {
        "Content-Type": "application/json", "Authorization": `bearer ${localStorage.getItem("AccessToken")}`,
        "Cache-Control": "no-cache",
        "Pragma": "no-cache"
      }
    }
  }

  protected handleError(httpErrorResponse: HttpErrorResponse): Observable<any> {
    if (httpErrorResponse.status === 401) {
      if (localStorage.getItem('AccessToken')) {
        localStorage.removeItem('AccessToken');
        localStorage.setItem('AuthorizationStatus', "UNAUTHORIZED");
      }
    }
    return null;
  }
}
