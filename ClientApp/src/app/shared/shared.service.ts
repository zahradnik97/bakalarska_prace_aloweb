import { Injectable, Inject, ChangeDetectorRef } from '@angular/core';
import { ApiConfig } from '../api.config';
import { IAbilityModel } from '../abilities/models/ability.model';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../core/api/api.service';
import { ICodeTableItem, CodeTableType } from './models/code-table.model';
import { IEmployeeAuth } from '../employees/models/employee.model';
import { Router } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { map } from 'rxjs/operators';

@Injectable()
export class SharedService extends ApiService {
  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string, router: Router) {
    super(http, baseUrl, router);
  }

  public user: IEmployeeAuth = { Roles:["Employee"]};
  public loading: boolean = false;
  public passwordChanged: boolean = true;
  public roles: string[] = ["Administrator", "Employee", "SalesManager", "ProjectManager", "HRManager"];
  public requestTypesCodeTable: ICodeTableItem[] = [
    { id: 1, name: "HomeOffice" },
    { id: 2, name: "Vacation" },
    { id: 3, name: "NoPayVacation" },
    { id: 4, name: "BussinessTrip" },
    { id: 5, name: "SubstituteVacation" }
  ];

  authorize() : Observable<any> {
    return super.get(ApiConfig.endpoints.authorization.authorize.url).pipe(
      map((res :any) => {
        let mappedRes: IEmployeeAuth = {
          Roles: res.roles,
          Email: res.email,
          EmployeeId: res.employeeId,
          FirstName: res.firstName,
          LastName: res.lastName
        };
        return mappedRes;
      })
    );
  }

  getProjectManagerCodeTable(): Observable<ICodeTableItem[]>{
    return super.get(ApiConfig.endpoints.codeTables.projectManagers.url);
  }

  showLoader() {
    setTimeout(() => {
      this.loading = true;
    }, 0);
  }

  disableLoader() {
    setTimeout(() => {
      this.loading = false;
    }, 0);
  }

  isUserLogged() {
    if (!localStorage.getItem('AccessToken') && !(['/login', '/'].includes(this.router.url))  ) {
      this.logOut();
    }
    return !!localStorage.getItem('AccessToken');
  }

  logOut() {
    localStorage.removeItem('AccessToken');
    this.user = {};
    this.router.navigate(['/login']);
  }

  getCodeTable$(name: CodeTableType): Observable<ICodeTableItem[]> {
    return super.get(ApiConfig.endpoints.codeTables[name].url)
  }

  getCodeTable(type: string): ICodeTableItem[] {
    this.getCodeTable$(CodeTableType[type]).subscribe((res) => {
      return res;
    }, (e) => {
        return [];
    });
    return [];
  }

  generateForm(formInterface) {
    let properties = Object.keys(formInterface);
    let group = {};
    properties.forEach((property) => {
      group[property] = new FormControl();
    });
    return new FormGroup(group);
  }

  patchForm(form: FormGroup, value: any) {
    if (value) {
      Object.keys(value).forEach(name => {
        if (form.controls[name]) {
          form.controls[name].patchValue(value[name], { onlySelf: true, emitEvent: true });
        }
      });
      form.updateValueAndValidity({ onlySelf: true, emitEvent: true });
    }
  }

  getControlValidation(control: FormControl) {
    return control.invalid && (control.dirty || control.touched)
  }

  markFormGroupTouched(formGroup: FormGroup) {
    (<any>Object).values(formGroup.controls).forEach(control => {
      control.markAsTouched();

      if (control.controls) {
        this.markFormGroupTouched(control);
      }
    });
  }

}
