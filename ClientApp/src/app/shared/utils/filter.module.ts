import { NgModule } from '@angular/core';
import { FilterPipe } from '../utils/filter.pipe';
import { CodeTablePipe } from '../../core/pipes/codetable.pipe';

@NgModule({
  declarations: [FilterPipe,CodeTablePipe ],
    
  exports: [FilterPipe, CodeTablePipe]
})

export class FilterModule { }
