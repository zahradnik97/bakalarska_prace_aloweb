import { Pipe, PipeTransform } from '@angular/core';
import { FilterEmployeeModel } from '../../employees/models/employee.model';
import { FilterAbilityModel } from '../../abilities/models/ability.model';
import { FilterProject } from '../models/project.model';

@Pipe({
  name: 'fulltextFilter'
})
export class FilterPipe implements PipeTransform {

  filterEmployee = { firstName: "", position: "", lastName: "", abilities: null, name: "", fullName: "", abilitiesFilter : "" };
  filterAbility: FilterAbilityModel = { name: "" };
  filterProject = { client: "", name: "", note: "", projectManagerFilter: "" };
  filterRequest = {fullName: "" , firstName:"", lastName: ""}
  dashboardFilter = { employee: null, firstName: "", position: "", lastName: "", fullName: "", abilitiesFilter: "" }

  employeeFields: string[] = Object.keys(this.filterEmployee);
  abilitiesFields: string[] = Object.keys(this.filterAbility);
  projectsFields: string[] = Object.keys(this.filterProject);
  dashboardFields: string[] = Object.keys(this.dashboardFilter);
  requestFields: string[] = Object.keys(this.filterRequest);

  transform(items: any[], component: string, args: any): any[] {
    if (args === '') {
      return items;
    }

    if (component === 'dashboardFilter') {
      items.forEach((item) => {
        item.fullName = item.employee.firstName + " " + item.employee.lastName;
        item.employee.abilities.forEach((ability) => {
          item.abilitiesFilter += ability.name + " ";
        });
      })
      console.log(items);
    }
    else if (component === 'employeesFilter') {
      items.forEach((item) => {
        item.fullName = item.firstName + " " + item.lastName;
        item.abilities.forEach((ability) => {
          item.abilitiesFilter += ability.name + " ";
        });
      });
    }
    else if (component === 'projectsFilter') {
      items.forEach((item) => {
        item.projectManagerFilter = item.projectManager.firstName + " " + item.projectManager.lastName;
      });
      console.log(items);
    }
    else if (component === 'requestsFilter') {
      items.forEach((item) => {
        item.fullName = item.firstName + " " + item.lastName;
      });
      console.log(items);
    }

    const isSearch = (data: any): boolean => {
      let isAll = false;
      if (data === undefined || data === null) {
        return isAll;
      }
      if (typeof data === 'object') {
        if (data.name != undefined || data.name != null) {
          isAll = isSearch(data['name']);
        }
        if (component === 'employeesFilter') {
          for (const z in this.employeeFields) {
            if (isAll = isSearch(data[this.employeeFields[z]])) {
              break;
            }
          }
        }
        else if (component === 'requestsFilter') {
          for (const z in this.requestFields) {
            if (isAll = isSearch(data[this.requestFields[z]])) {
              break;
            }
          }
        }
        else if (component === 'abilitiesFilter') {
          for (const z in this.abilitiesFields) {
            if (isAll = isSearch(data[this.abilitiesFields[z]])) {
              break;
            }
          }
        }
        else if (component === 'projectsFilter') {
          for (const z in this.projectsFields) {
            if (isAll = isSearch(data[this.projectsFields[z]])) {
              break;
            }
          }
        }
        else if (component === 'dashboardFilter') {
          for (const z in this.dashboardFields) {
            if (isAll = isSearch(data[this.dashboardFields[z]])) {
              break;
            }
          }
        }
      } else {
        if (typeof args === 'number') {
          isAll = data === args;
        } else {
          isAll = data.toString().toLowerCase().indexOf(args.toLowerCase()) !== -1;
        }
      }

      return isAll;
    };

    return items.filter(isSearch);
  }
}
