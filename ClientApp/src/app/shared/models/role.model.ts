export enum RoleType {
  Administrator = "Administrator",
  Employee = "Employee",
  SalesManager = "SalesManager",
  HRManager = "HRManager",
  ProjectManager = "ProjectManager"
}
