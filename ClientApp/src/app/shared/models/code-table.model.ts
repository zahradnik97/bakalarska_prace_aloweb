export interface ICodeTableItem {
  id: number;
  name: string;
}


export class CodeTableItem implements ICodeTableItem {
  id: number;
  name: string;
}

export enum CodeTableType {
  abilities = "abilities",
  projects = "projects",
  employees = "employees"
}
