export interface IProject {
  projectId?: number;
  name?: string;
  projectManagerId?: number;
  isBillable?: boolean;
  color?: string;
  client?: string;
  note?: string;
}

export class Project implements IProject {
  name= "";
  projectManagerId= 0;
  isBillable= null;
  color= "";
  client= "";
  note= "";
}

export class FilterProject {
  name?: string;
  projectManager?: any;
  client?: string;
  note?: string;
}
