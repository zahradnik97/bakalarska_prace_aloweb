export interface IChangePasswordModel {
  employeeId?: number,
  oldPassword?: string,
  newPassword?: string
}
