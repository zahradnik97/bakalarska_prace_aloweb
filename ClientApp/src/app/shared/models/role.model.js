"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var RoleType;
(function (RoleType) {
    RoleType["Administrator"] = "Administrator";
    RoleType["Employee"] = "Employee";
    RoleType["SalesManager"] = "SalesManager";
    RoleType["HRManager"] = "HRManager";
    RoleType["ProjectManager"] = "ProjectManager";
})(RoleType = exports.RoleType || (exports.RoleType = {}));
//# sourceMappingURL=role.model.js.map