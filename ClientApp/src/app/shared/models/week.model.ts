import { IDay } from "./day.model";

export interface IWeek{
  weekId: number;
  dateFrom: string;
  dateTo: string;
  year: string; // ????
  //days: IDay[];
}

export class Week implements IWeek {
  weekId: number;
  dateFrom: string;
  dateTo: string;
  year: string; // ????
  //days: IDay[];
}
