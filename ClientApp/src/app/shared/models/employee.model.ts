export interface IEmployee {
  employeeId?: number;
  firstName?: string;
  lastName?: string;
  abilityId?: number;
  teamId?: number; // ???? bude potreba
  typeOfContract?: number; // 0.75 etc.
  review?: number; // ??? pridat hodnoceni?
  position?: string;

}

export class Employee implements IEmployee {

}
