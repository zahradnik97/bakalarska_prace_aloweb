export interface IAbility {
  abilityId: number;
  name: string;
}

export class Ability implements IAbility {
  abilityId: number;
  name: string;
}
