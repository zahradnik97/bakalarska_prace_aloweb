import { Employee, IEmployee } from "./employee.model";
import { IProject } from "./project.model";
import { IWeek } from "./week.model";
import { IDay } from "./day.model";
import { DashboardDay } from "./dashboard-models/dashboard-day.model";

export interface IDashboardItem {
  employee?: IEmployee;
  days?: DashboardDay[];
  weekId?: number;
  endDate?: string;
  startDate?: string;
}

export class DashboardItem implements IDashboardItem {

}
