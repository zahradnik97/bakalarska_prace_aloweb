import { IProject } from "../project.model";

export interface IDashboardProject {
  allocation: number;
}

export class DashboardProject implements IDashboardProject, IProject {
  projectId: number;
  name: string;
  projectManagerId: number;
  isBillable: boolean;
  color: string;
  allocation: number;
}
