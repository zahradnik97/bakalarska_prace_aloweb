import { IDay } from "../day.model";
import { DashboardProject } from "./dashboard-project.model";

export interface IDashboardDay extends IDay {
  projects?: DashboardProject[];
}

export class DashboardDay implements IDashboardDay {
  
}
