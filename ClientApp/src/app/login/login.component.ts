import { Component, OnInit, AfterViewChecked, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { LoginService } from './login.service';
import { ILoginResponseModel } from './models/login-response.model';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { ILoginModel } from './models/login.model';
import { Router } from '@angular/router';
import { SharedService } from '../shared/shared.service';

declare var swal: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, AfterViewInit {

  loginForm: FormGroup;
  errorString: string;
  errorResponse: boolean = false;

  constructor(private loginService: LoginService,
    private formBuilder: FormBuilder,
    private router: Router,
    private sharedService: SharedService)
  { }

  ngOnInit() {
    if (this.sharedService.isUserLogged()) {
      this.router.navigate(['/dashboard']);
    }
    if (localStorage.getItem("AuthorizationStatus") == "UNAUTHORIZED") {
      swal({
        icon: 'warning',
        title: `Autentizace`,
        text: 'Přihlašte se znovu prosím. Byl/a jste automaticky odhlášen/a',
      });
      localStorage.removeItem("AuthorizationStatus");
    }

    this.buildForm();
  }

  buildForm() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  onSubmit(loginValues: ILoginModel) {
    console.log(this.loginForm);
    if (!this.loginForm.valid) {
      this.sharedService.markFormGroupTouched(this.loginForm);
      return;
    }
    this.sharedService.showLoader();
    this.loginService.$login(loginValues).subscribe((res: ILoginResponseModel) => {
      if (res.token) {
        localStorage.setItem("AccessToken", res.token);
        //let jsonObject = JSON.parse(atob(localStorage.getItem("AccessToken").split('.')[1]));
        //this.sharedService.user = JSON.parse(jsonObject[Object.keys(jsonObject)[0]]);
        this.sharedService.user = { Email: res.user.email, EmployeeId: res.user.employeeId, FirstName: res.user.firstName, LastName: res.user.lastName,Roles: res.user.roles };
        this.router.navigate(['/dashboard']);
        setTimeout(() => {
          this.sharedService.passwordChanged = res.passwordChanged;
        }, 0);
      }
      else {
        this.errorResponse = true;
      }
      this.sharedService.disableLoader();
      console.log(res);
    }, (e) => {
        this.sharedService.disableLoader();
        this.errorResponse = true;
    });
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.sharedService.disableLoader();
      localStorage.removeItem("AuthorizationStatus");
    }, 1000)

  }


}
