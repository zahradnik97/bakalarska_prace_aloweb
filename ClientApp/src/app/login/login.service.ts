import { Injectable, Inject } from '@angular/core';
import { ApiService } from '../core/api/api.service';
import { HttpClient } from '@angular/common/http';
import { ApiConfig } from '../api.config';
import { ILoginResponseModel } from './models/login-response.model';
import { Observable } from 'rxjs/Observable';
import { ILoginModel } from './models/login.model';
import { IChangePasswordModel } from '../shared/models/change-password.model';

@Injectable()
export class LoginService extends ApiService {

  $login(loginModel: ILoginModel): Observable<ILoginResponseModel> {
    return super.post(ApiConfig.endpoints.authorization.login.url, loginModel)
  }

  $changePassword(changePasswordModel: IChangePasswordModel): Observable<any> {
    return super.put(ApiConfig.endpoints.authorization.changePassword.url, changePasswordModel)
  }

  $resetPassword(employeeId: number): Observable<any> {
    return super.put(ApiConfig.endpoints.authorization.resetPassword.url + `/${employeeId}`, null)
  }

}
