import { IEmployee } from "../../shared/models/employee.model";
import { IEmployeeAuth } from "../../employees/models/employee.model";

export interface ILoginResponseModel {
  user?: any;
  token?: string;
  passwordChanged?: boolean;
}
