"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var WeekInfo = /** @class */ (function () {
    function WeekInfo(week) {
        this.weekId = week.weekId;
        this.startDate = week.startDate;
        this.endDate = week.endDate;
        this.weekNumber = parseInt(this.weekId.toString().substr(4, this.weekId.toString().length - 1));
        this.weekYear = this.weekId.toString().substr(0, 4);
    }
    WeekInfo.prototype.getWeekInfo = function () {
        return this.weekNumber + '. Týden ' + this.startDate + ' - ' + this.endDate;
    };
    return WeekInfo;
}());
exports.WeekInfo = WeekInfo;
//# sourceMappingURL=week-info.model.js.map