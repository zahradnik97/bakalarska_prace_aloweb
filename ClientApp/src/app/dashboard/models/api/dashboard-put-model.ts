export interface IDashboardPutModel{
  dayId: number,
  projectId: number,
  employeeId: number,
  allocation: number
}
