import { IDashboardDay } from "../../../shared/models/dashboard-models/dashboard-day.model";
import { IEmployee } from "../../../shared/models/employee.model";

export interface IDashboardItemResponse {
  weekId?: number;
  dayOne?: IDashboardDay;
  dayTwo?: IDashboardDay;
  dayThree?: IDashboardDay;
  dayFour?: IDashboardDay;
  dayFive?: IDashboardDay;
  employee?: IEmployee;
  startDate?: string;
  endDate?: string;

}

export class DashboardItemResponse implements IDashboardItemResponse {

}
