export class WeekInfo {

  weekId: number;
  startDate: string;
  endDate: string;
  weekNumber: number;
  weekYear: string;

  constructor(week: any) {
    this.weekId = week.weekId;
    this.startDate = week.startDate;
    this.endDate = week.endDate;
    this.weekNumber = parseInt(this.weekId.toString().substr(4, this.weekId.toString().length - 1));
    this.weekYear = this.weekId.toString().substr(0, 4);
  }

}
