import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { DashboardService } from './dashboard.service';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { FilterModule } from '../shared/utils/filter.module';
import { ApiService } from '../core/api/api.service';
import { DashboardEditComponent } from './edit/dashboard-edit.component';

@NgModule({
  imports: [
    CommonModule,
    FilterModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [DashboardComponent, DashboardEditComponent],
  exports: [DashboardComponent],
  providers: [DashboardService, ApiService]
})
export class DashboardModule { }
