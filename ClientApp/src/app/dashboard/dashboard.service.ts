import { Injectable, Inject } from '@angular/core';
import { ApiConfig } from '../api.config';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { ApiService } from '../core/api/api.service';
import { IDashboardItemResponse, DashboardItemResponse } from './models/api/dashboard-item-response';
import { map, catchError } from 'rxjs/operators'
import { IDashboardItem, DashboardItem } from '../shared/models/dashboard-item.model';
import { Router } from '@angular/router';
import { IDashboardPutModel } from './models/api/dashboard-put-model';
import { IProject } from '../shared/models/project.model';
import { RoleType } from '../shared/models/role.model';
import { SharedService } from '../shared/shared.service';
import { IEmployeeAuth } from '../employees/models/employee.model';

@Injectable()
export class DashboardService extends ApiService {

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string, router: Router) {
    super(http, baseUrl,router);
  }

  getDay(dayIndex) {
    let days = ["pondělí", "úterý", "středa", "čtvrtek", "pátek"];
    return days[dayIndex];
  }

  $getAllocations(weekId: number): Observable<any[]> {
    return super.getById(ApiConfig.endpoints.dashboard.url, weekId);
  }

  $putAllocation(dashboardModel: IDashboardPutModel[]): Observable<any> {
    return super.put(ApiConfig.endpoints.dashboard.url, dashboardModel);
  }

  $getProjects(user: IEmployeeAuth): Observable<IProject[]> {
    return super.get(ApiConfig.endpoints.projects.url).pipe(map(
      (res: IProject[]) => {
        if (user.Roles.includes(RoleType.Administrator)) {
          return res;
        }
        else {
          let mappedRes = [];
          res.forEach((project) => {

            if (project.projectManagerId === user.EmployeeId) {
              mappedRes.push(project);
            }
          });

          return mappedRes;
        }
    }));
  }

  
}
