import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IDashboardPutModel } from '../models/api/dashboard-put-model';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { DashboardService } from '../dashboard.service';
import { SharedService } from '../../shared/shared.service';
import { ICodeTableItem, CodeTableType } from '../../shared/models/code-table.model';
import { m } from '@angular/core/src/render3';
import { fork } from 'cluster';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { IProject } from '../../shared/models/project.model';
import { RoleType } from '../../shared/models/role.model';

declare var swal: any;

@Component({
  selector: 'app-dashboard-edit',
  templateUrl: './dashboard-edit.component.html',
  styleUrls: ['./dashboard-edit.component.css']
})
export class DashboardEditComponent implements OnInit {

  item: any = {};

  @Input()
  set inputItem(value) {
    Object.assign(this.item, value);
    if (this.item.week) {
      this.getProjects();
    }
  };
  form: FormGroup;
  projectCodeTable: IProject[] = [];
  formGroupDays = ["monday", "tuesday", "wednesday", "thursday", "friday"];
  @Output() onModificationComplete: EventEmitter<any> = new EventEmitter();
  updatedDays: IDashboardPutModel[] = [];
  

  constructor(private formBuilder: FormBuilder,
    private dashboardService: DashboardService,
    private sharedService: SharedService) { }

  getProjects() {
    this.updatedDays = [];
    this.item.week.days.forEach((day) => {
      day.projects.forEach((project) => {
        this.updatedDays.push({
          allocation: project.allocation,
          dayId: day.dayId,
          employeeId: this.item.employee.employeeId,
          projectId: project.projectId
        });
      });
    });
    console.log(this.updatedDays);
  }

  ngOnInit() {
    this.getCodeTables();
    this.form = this.formBuilder.group({
      monday: this.formBuilder.group({
        projectId: [null, Validators.required],
        allocation: [null, Validators.required],
        employeeId: new FormControl(),
        dayId: new FormControl()
      }),
      tuesday: this.formBuilder.group({
        projectId: [null, Validators.required],
        allocation: [null, Validators.required],
        employeeId: new FormControl(),
        dayId: new FormControl()
      }),
      wednesday: this.formBuilder.group({
        projectId: [null, Validators.required],
        allocation: [null, Validators.required],
        employeeId: new FormControl(),
        dayId: new FormControl()
      }),
      thursday: this.formBuilder.group({
        projectId: [null, Validators.required],
        allocation: [null, Validators.required],
        employeeId: new FormControl(),
        dayId: new FormControl()
      }),
      friday: this.formBuilder.group({
        projectId: [null, Validators.required],
        allocation: [null, Validators.required],
        employeeId: new FormControl(),
        dayId: new FormControl()
      }),
    });
  }

  getDay(dayIndex) {
    return this.formGroupDays[dayIndex];
  }

  getCodeTables() {
    this.dashboardService.$getProjects(this.sharedService.user).subscribe((res) => {
      this.projectCodeTable = res;
    }, (e) => {
    })
  }

  canDelete(projectId) {
    for (let project of this.projectCodeTable) {
      if (project.projectId === projectId) {
        return true;
      }
    }
    return false;
  }

  modifyAllocation() {
    this.sharedService.showLoader();
    this.dashboardService.$putAllocation(this.updatedDays).subscribe((res) => {
      this.sharedService.disableLoader();
      this.onModificationComplete.emit(null);
      this.form.reset();
      (document.getElementsByClassName('dismiss-modal')[0] as any).click();
      swal({
        icon: 'success',
        title: `Alokace zaměstnance`,
        text: `Alokace byla úspěšně upravena.`,
        timer: 1300
      });
    }, (e) => {
        this.sharedService.disableLoader();
    });
  }

  addProject(day, formGroup: FormGroup) {
    if (!formGroup.valid) {
      this.sharedService.markFormGroupTouched(formGroup);
      return;
    }
    let countAllocation = 0;
    this.updatedDays.forEach((item) => {
      if (day.dayId == item.dayId) {
        countAllocation += +item.allocation;
      }
    });
    if ((countAllocation + parseInt(formGroup.value.allocation)) <= this.item.employee.contractType) { //contract type
        let include = false;
        for (let project of this.updatedDays) {
          if (+project.projectId === +formGroup.value.projectId && day.dayId == project.dayId) {
            project.allocation += parseInt(formGroup.value.allocation);
            include = true;
            break;
          }
        };
        if (!include) {
          this.updatedDays.push(
            {
              projectId: +formGroup.value.projectId,
              allocation: +formGroup.value.allocation,
              employeeId: +this.item.employee.employeeId,
              dayId: +day.dayId
            });
        }
    }
    else {
      let contractType = this.item.employee.contractType;
      swal({
        icon: 'error',
        title: `Překročení alokace`,
        text: `Den je alokován ${countAllocation}H a zaměstnanec má pracovní dobu ${this.item.employee.contractType}H na den`,
      });
    }

    formGroup.reset();
  }

  deleteProject(project, day) {
     //CHECK
      for (let item of this.updatedDays) {
        if (item.projectId == project.projectId && item.dayId == day.dayId) {
          item.allocation = 0;
          for (let itemProject of day.projects) {
            if (itemProject.projectId == project.projectId) {
              itemProject.allocation = 0;
            }
          }
          break;
        }
      }
  }

  getHoursTransformation(value) {
    switch (value) {
      case 1:
        return value + ' hodina';
      case 2:
      case 3:
      case 4:
        return value + ' hodiny';
      case 5:
      case 6:
      case 7:
      case 8:
        return value + ' hodin';
    }
  }

}
