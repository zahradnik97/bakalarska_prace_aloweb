import { Component, OnInit, HostListener } from '@angular/core';
import { DashboardService } from './dashboard.service';
import { IDashboardItem } from '../shared/models/dashboard-item.model';
import { WeekInfo } from './models/week-info.model';
import { Router } from '@angular/router';
import { IDashboardPutModel } from './models/api/dashboard-put-model';
import { SharedService } from '../shared/shared.service';
declare const getWeekNumber: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  editModel: IDashboardPutModel[] = [];
  employeesFilterInput: string = "";
  weekInfo: WeekInfo;
  endYear = false;
  startYear = false;
  header: HTMLElement;
  stickyPosition: number;
  @HostListener('window:scroll', ['$event'])
  scrollHandler(event) {
    if (window.pageYOffset > this.stickyPosition) {
      this.header.classList.add("sticky");
    } else {
      this.header.classList.remove("sticky");
    }
  }

  constructor(private dashboardService: DashboardService,
    private router: Router,
    private sharedService: SharedService) { }
  private week: any[] = [];
  ngOnInit() {
    this.header = document.getElementById("stickyHeader");
    this.stickyPosition = this.header.offsetTop;
    this.getWeekAllocation(parseInt(new Date().getFullYear().toString() + getWeekNumber()));
  }

  getWeekAllocation(weekId, nextWeek?: boolean) {
    if (nextWeek !== undefined) {
      this.sharedService.showLoader();

      this.dashboardService.$getAllocations(nextWeek ? parseInt(this.weekInfo.weekYear + (this.weekInfo.weekNumber + 1).toString())
        : parseInt(this.weekInfo.weekYear + (this.weekInfo.weekNumber - 1).toString())).subscribe((res) => {
        this.week = res;
          this.weekInfo = new WeekInfo(res[0].week);
          this.endYear = this.weekInfo.weekNumber === 53;
          this.startYear = this.weekInfo.weekNumber === 1;

        this.sharedService.disableLoader();
      }, (e) => {
          this.sharedService.disableLoader();
        this.router.navigate(['/login']);
      });
    }
    else {
      this.sharedService.showLoader();
      this.dashboardService.$getAllocations(weekId as number).subscribe((res) => {
        this.week = res;
        this.weekInfo = new WeekInfo(res[0].week);
        this.sharedService.disableLoader();
      }, (e) => {
          this.sharedService.disableLoader();
        this.router.navigate(['/login']);
      });
    }
  }

  openEditModal() {
    document.getElementById('dashboardModal')
  }

  setEditModel(model) {
    this.editModel = model;
  }

  onModificationComplete() {
    this.getWeekAllocation(this.weekInfo.weekId);
  }

}
