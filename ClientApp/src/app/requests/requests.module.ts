import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RequestsComponent } from './requests.component';
import { RequestsService } from './requests.service';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { FilterModule } from '../shared/utils/filter.module';
import { RequestTypePipe } from './request-type.pipe';
import { RequestsListComponent } from './requests-list/requests-list.component';

@NgModule({
  imports: [
    CommonModule,
    FilterModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [RequestsComponent, RequestTypePipe, RequestsListComponent],
  providers: [RequestsService]
})
export class RequestsModule { }
