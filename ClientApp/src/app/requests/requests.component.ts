import { Component, OnInit, HostListener, AfterViewInit } from '@angular/core';
import { SharedService } from '../shared/shared.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { IRequest, RequestType } from './request.model';
import { RequestsService } from './requests.service';
import { ICodeTableItem, CodeTableType } from '../shared/models/code-table.model';
import { RoleType } from '../shared/models/role.model';

declare var swal: any;

@Component({
  selector: 'app-requests',
  templateUrl: './requests.component.html',
  styleUrls: ['./requests.component.css']
})
export class RequestsComponent implements OnInit, AfterViewInit {
    ngAfterViewInit(): void {
      this.detectHeader('employeeRequests');
    }

  addNewForm: FormGroup;
  editForm: FormGroup;

  requestFilterInput = "";
  employeeRequests: IRequest[] = [];
  employeeRequestsAll: IRequest[] = [];
  hrmRequests: IRequest[] = [];
  hrmRequestsAll: IRequest[] = [];
  pmRequests: IRequest[] = [];
  pmRequestsAll: IRequest[] = []
  projectManagersCodeTable: ICodeTableItem[];

  requestToEdit: IRequest;

  header: HTMLElement;
  stickyPosition: number;
  showOutdated: boolean = false;
  @HostListener('window:scroll', ['$event'])
  scrollHandler(event) {
    if (window.pageYOffset > this.stickyPosition && this.header ) {
      this.header.classList.add("sticky");
    } else {
      this.header.classList.remove("sticky");
    }
  }

  constructor(private sharedService: SharedService,
    private fb: FormBuilder,
    private requestsService: RequestsService
  ) { }

  ngOnInit() {
    this.buildForms();
    this.getCodeTables();
    this.getData();
  }

  detectHeader(header) {
    setTimeout(() => {
      this.header = document.getElementById(header);
      this.stickyPosition = this.header.offsetTop;
    }, 1000);
  }

  filterByType($event) {
    if ($event.value) {
      let newArray: IRequest[] = [];
      this[$event.array + 'All'].forEach((item) => {
        if (RequestType[item.type] == $event.value) {
          newArray.push(item);
        }
      });
      this[$event.array] = [];
      Object.assign(this[$event.array],newArray);
      this[$event.array].splice(0, 0);
    }
    else {
      Object.assign(this[$event.array], this[$event.array + "All"]);
      this[$event.array].splice(0, 0);
    }
  }

  getData() {
    this.sharedService.showLoader();
    if ((this.sharedService.user.Roles.includes(RoleType.ProjectManager) || this.sharedService.user.Roles.includes(RoleType.Administrator))) {
      this.requestsService.getPMRequests$(this.sharedService.user.EmployeeId).subscribe((res) => {
        this.pmRequests = res;
        Object.assign(this.pmRequestsAll, res);
      }, (e) => {
      });
    }
    if ((this.sharedService.user.Roles.includes(RoleType.HRManager) || this.sharedService.user.Roles.includes(RoleType.Administrator))) {
      this.requestsService.getHRRequests$().subscribe((res) => {
        this.hrmRequests = res;
        Object.assign(this.hrmRequestsAll, res);
      }, (e) => {
      });
    }
    this.requestsService.getRequests$(this.sharedService.user.EmployeeId).subscribe((res) => {
      this.employeeRequests = res;
      Object.assign(this.employeeRequestsAll, res);
      this.sharedService.disableLoader();
    }, (e) => {
        this.sharedService.disableLoader();
    });
  }

  getCodeTables() {
    this.sharedService.getProjectManagerCodeTable().subscribe((res) => {
      this.projectManagersCodeTable = res;
    }, (e) => {
    });
  }

  buildForms() {
    this.addNewForm = this.fb.group({
      dateTo: [null, Validators.required],
      dateFrom: [null, Validators.required],
      projectManagerId: [null, Validators.required],
      type: [null, Validators.required],
      reason: [null]
    });

    this.editForm = this.fb.group({
      dateTo: [null, Validators.required],
      dateFrom: [null, Validators.required],
      projectManagerId: [null, Validators.required],
      type: [null, Validators.required],
      reason: [null]
    });
  }

  changeDatepicker(form: FormGroup, field: string, input) {
    setTimeout(() => {
      form.get(field).setValue(input.value);
    }, 400);
  }

  addNewRequest() {
    if (!this.addNewForm.valid) {
      this.sharedService.markFormGroupTouched(this.addNewForm);
      return;
    }

    if (this.validateDateRange(this.addNewForm.value.dateFrom, this.addNewForm.value.dateTo)) {
      swal({
        icon: 'error',
        title: `Vytvoření žádosti`,
        text: 'Datum do nesmí být menší než datum od.',
      });
      return;
    }

    const putModel: IRequest = {
      ...this.addNewForm.value,
      employeeId: this.sharedService.user.EmployeeId
    };
    console.log(putModel);
    this.sharedService.showLoader();
    this.requestsService.createRequest$(putModel).subscribe((res) => {
      this.getData();
      console.log(res);
      this.addNewForm.reset();
      (document.getElementsByClassName('dismiss-modal-add')[0] as any).click();
      this.sharedService.disableLoader();

      swal({
        icon: 'success',
        title: `Vytvoření žádosti`,
        text: `Žádost byla úspěšně vytvořena.`,
        timer: 1300
      });
    }, (e) => {
        this.sharedService.disableLoader();

    });
  }

  approveRequest($event) {
    let approve = $event.approve;

    swal({
      title: `${approve ? "Schválení" : "Zamítnutí"} žádosti`,
      text: `Chcete opravdu ${approve ? "schválit" : "zamítnout"} žádost?`,
      icon: 'warning',
      dangerMode: true,
      buttons: ["Zrušit", `${approve ? "Schválit" : "Zamítnout"}`],
    }).then(userInput => {
      if (userInput) {
        let putModel = {
          ...$event.requestModel
        };
        this.sharedService.showLoader();
        if ((this.sharedService.user.Roles.includes(RoleType.ProjectManager) || this.sharedService.user.Roles.includes(RoleType.Administrator)) && $event.role === "pmRequests") {
          putModel.isApprovedByPM = $event.approve;
          this.requestsService.approvePMRequest$(putModel).subscribe((res) => {
            swal({
              icon: 'success',
              title: `${approve ? "Schválení" : "Zamítnutí"} žádosti`,
              text: `Žádost byla ${approve ? "Schválena" : "Zamítnuta"}`,
              timer: 1300
            });
            this.sharedService.disableLoader();
            console.log(res);
            this.getData();
          }, (e) => {
              this.sharedService.disableLoader();
          });
        }
        else if ((this.sharedService.user.Roles.includes(RoleType.HRManager) || this.sharedService.user.Roles.includes(RoleType.Administrator)) && $event.role === "hrmRequests") {
          putModel.isApprovedByHRM = $event.approve;
          this.requestsService.approveHRRequest$(putModel).subscribe((res) => {
            console.log(res);
            this.getData();
            this.sharedService.disableLoader();

            swal({
              icon: 'success',
              title: `${approve ? "Schválení" : "Zamítnutí"} žádosti`,
              text: `Žádost byla ${approve ? "Schválena" : "Zamítnuta"}`,
              timer: 1300
            });

          }, (e) => {
              this.sharedService.disableLoader();
          });
        }

      }
    });

  }

  confirmRequest(requestModel: IRequest) {
    const updateModel: IRequest = { ...requestModel, isConfirmed: true };
    this.sharedService.showLoader();
    this.requestsService.putRequest$(updateModel).subscribe((res) => {
      this.getData();
      this.sharedService.disableLoader();

      swal({
        icon: 'success',
        title: `Potvrzení žádosti`,
        text: `Žádost byla úspěšně potvrzena.`,
        timer: 1300
      });
    }, (e) => {
        this.sharedService.disableLoader();

    });
  }

  deleteRequest(requestModel: IRequest) {
    swal({
      title: 'Odstranění žádosti',
      text: "Chcete opravdu odstranit žádost?",
      icon: 'warning',
      dangerMode: true,
      buttons: ["Zrušit", "Odstranit"],
    }).then(userInput => {
      if (userInput) {
        this.sharedService.showLoader();
        this.requestsService.deleteRequest$(requestModel.requestId, requestModel.employeeId).subscribe((res) => {
          console.log(res);
          this.getData();
          this.sharedService.disableLoader();

          swal({
            icon: 'success',
            title: 'Odstraněno',
            text: 'Žádost byla odstraněna',
            timer: 1300
          });

        }, (e) => {
            this.sharedService.disableLoader();

        });
      }
    });
  }

  setRequestToEdit(requestModel: IRequest) {
    this.requestToEdit = requestModel;
    this.sharedService.patchForm(this.editForm, this.requestToEdit);
  }

  editRequest() {

    if (!this.editForm.valid) {
      this.sharedService.markFormGroupTouched(this.editForm);
      return;
    }
    if (this.validateDateRange(this.editForm.value.dateFrom, this.editForm.value.dateTo)) {
      swal({
        icon: 'error',
        title: `Úprava žádosti`,
        text: `Datum do nesmí být menší než datum od.`,
      });
      return;
    }

    const putModel: IRequest = {
      ...this.requestToEdit,
      ...this.editForm.value
    };
    console.log(putModel);
    this.sharedService.showLoader();
    this.requestsService.putRequest$(putModel).subscribe((res) => {
      this.getData();
      this.editForm.reset();
      this.sharedService.disableLoader();
      (document.getElementsByClassName('dismiss-modal-edit')[0] as any).click();
      swal({
        icon: 'success',
        title: `Úprava žádosti`,
        text: `Žádost byla úspěšně upravena.`,
        timer: 1300
      });
      console.log(res);
    }, (e) => {
        this.sharedService.disableLoader();
    });
  }

  validateDateRange(dateFrom, dateTo) {
    return this.requestsService.convertDate(dateFrom) > this.requestsService.convertDate(dateTo);
  }

}
