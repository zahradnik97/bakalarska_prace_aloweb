export interface IRequest{
  requestId: number;
  dateTo: string;
  dateFrom: string;
  reason: string;
  projectManagerId: number;
  employeeId: number;
  type: string;
  isApprovedByPM: boolean;
  isApprovedByHRM: boolean;
  isConfirmed: boolean;
  firstName?: string;
  lastName?: string;
  outDated?: boolean;
}

export enum RequestType {
  HomeOffice = "Práce z domova",
  Vacation = "Dovolená",
  NoPayVacation = "Neplacené volno",
  BussinessTrip = "Pracovní cesta",
  SubstituteVacation = "Náhradní volno"
}
