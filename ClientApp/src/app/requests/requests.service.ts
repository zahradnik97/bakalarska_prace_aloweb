import { Injectable, Inject } from '@angular/core';
import { ApiConfig } from '../api.config';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../core/api/api.service';
import { Router } from '@angular/router';
import { IRequest } from './request.model';
import { map } from 'rxjs/operators';

@Injectable()
export class RequestsService extends ApiService {
  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string, router: Router) {
    super(http, baseUrl, router);
  }

  createRequest$(requestModel: IRequest): Observable<IRequest> {
    return super.post(ApiConfig.endpoints.requests.employee.url, requestModel);
  }
  deleteRequest$(requestId: number, employeeId:number): Observable<any> {
    return super.deleteById(ApiConfig.endpoints.requests.employee.url + `/${requestId}`, employeeId);
  }
  putRequest$(requestModel: IRequest): Observable <IRequest> {
    return super.put(ApiConfig.endpoints.requests.employee.url, requestModel);
  }
  getRequests$(employeeId: number): Observable<IRequest[]> {
    return super.getById(ApiConfig.endpoints.requests.employee.url, employeeId).pipe(map(
      (res: IRequest[]) => {
        res.sort((a, b) => {
          if (this.convertDate(a.dateFrom) > this.convertDate(b.dateFrom))
            return 1;
          else if (this.convertDate(a.dateFrom) < this.convertDate(b.dateFrom))
            return -1;
          else
            return 0;
        });
        res.forEach((item) => {
          item.outDated = this.convertDate(item.dateFrom) < new Date();
        });
        return res;
      }));
  }
  getHRRequests$(): Observable<IRequest[]> {
    return super.get(ApiConfig.endpoints.requests.HR.url).pipe(map(
      (res: IRequest[]) => {
        res.sort((a, b) => {
          if (this.convertDate(a.dateFrom) > this.convertDate(b.dateFrom))
            return 1;
          else if (this.convertDate(a.dateFrom) < this.convertDate(b.dateFrom))
            return -1;
          else
            return 0;
        });
        res.forEach((item) => {
          item.outDated = this.convertDate(item.dateFrom) < new Date();
        });
        return res;
    }));
  }
  approveHRRequest$(requestModel: IRequest): Observable<IRequest> {
    return super.put(ApiConfig.endpoints.requests.approveHR.url, requestModel);
  }
  getPMRequests$(projectManagerId: number): Observable<IRequest[]> {
    return super.getById(ApiConfig.endpoints.requests.PM.url, projectManagerId).pipe(map(
      (res: IRequest[]) => {
        res.sort((a, b) => {
          if (this.convertDate(a.dateFrom) > this.convertDate(b.dateFrom))
            return 1;
          else if (this.convertDate(a.dateFrom) < this.convertDate(b.dateFrom))
            return -1;
          else
            return 0;
        });
        res.forEach((item) => {
          item.outDated = this.convertDate(item.dateFrom) < new Date();
        });
        return res;
      }));
  }
  approvePMRequest$(requestModel: IRequest): Observable<IRequest> {
    return super.put(ApiConfig.endpoints.requests.approvePM.url, requestModel);
  }

  convertDate(date) {
    let dateToArray = date.split(".");
    return new Date(dateToArray[2] + "-" + dateToArray[1] + "-" + dateToArray[0])
  }
}
