import { Pipe, PipeTransform } from '@angular/core';
import { RequestType } from './request.model';

@Pipe({ name: 'requestTypePipe' })
export class RequestTypePipe implements PipeTransform {
  constructor() {
  }
  transform(value) {
    return RequestType[value];
  }

}


