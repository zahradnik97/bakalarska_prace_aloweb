import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IRequest } from '../request.model';
import { SharedService } from '../../shared/shared.service';

@Component({
  selector: 'app-requests-list',
  templateUrl: './requests-list.component.html',
  styleUrls: ['./requests-list.component.css']
})
export class RequestsListComponent implements OnInit {

  showOutdated = false;
  requestFilterInput = "";

  @Input() requestsList: IRequest[] = [];
  @Input() isManagerList: boolean;
  @Input() type: string = "";

  @Output() onApproveRequest: EventEmitter<any> = new EventEmitter();
  @Output() onFilterByType: EventEmitter<any> = new EventEmitter();
  @Output() onSetRequestToEdit: EventEmitter<any> = new EventEmitter();
  @Output() onConfirmRequest: EventEmitter<any> = new EventEmitter();
  @Output() onDeleteRequest: EventEmitter<any> = new EventEmitter();

  constructor(private sharedService: SharedService) { }

  ngOnInit() {
  }

  filterByType(value, array) {
    this.onFilterByType.emit({ value: value, array: array});
  }

  approveRequest(requestModel, approve, role) {
    this.onApproveRequest.emit({ requestModel: requestModel, approve: approve, role: role});
  }

  setRequestToEdit(request) {
    this.onSetRequestToEdit.emit(request);
  }

  deleteRequest(request) {
    this.onDeleteRequest.emit(request);
  }

  confirmRequest(request) {
    this.onConfirmRequest.emit(request);
  }

}
