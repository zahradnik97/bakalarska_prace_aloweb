"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AppConfig = /** @class */ (function () {
    function AppConfig() {
    }
    return AppConfig;
}());
exports.AppConfig = AppConfig;
exports.ApiConfig = {
    endpoints: {
        authorization: {
            login: { url: '/Auth/Login' },
            changePassword: { url: '/Auth/ChangePassword' },
            authorize: { url: '/Auth/Authorize' },
            resetPassword: { url: '/Auth/ResetPassword' }
        },
        dashboard: { url: '/Dashboard' },
        employees: { url: '/Employees' },
        abilities: { url: '/Abilities' },
        projects: { url: '/Projects' },
        requests: {
            employee: { url: '/Requests' },
            approveHR: { url: '/Requests/ApproveByHRM' },
            approvePM: { url: '/Requests/ApproveByPM' },
            HR: { url: '/Requests/GetHRRequests' },
            PM: { url: '/Requests/GetPMRequests' }
        },
        codeTables: {
            employees: { url: '/CodeTables/Employee' },
            projects: { url: '/CodeTables/Project' },
            abilities: { url: '/CodeTables/Ability' },
            projectManagers: { url: '/CodeTables/GetProjectManagers' }
        }
    }
};
//# sourceMappingURL=api.config.js.map