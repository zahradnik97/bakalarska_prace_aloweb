﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using zaht02_BP.Models.Dashboard;
using zaht02_BP.Models.Employees;
using zaht02_BP.Models.Projects;

namespace zaht02_BP.Helpers
{
    public static class Helper
    {
        public static string Hash(string input)
        {
            var hash = new SHA256Managed().ComputeHash(Encoding.UTF8.GetBytes(input));
            return string.Concat(hash.Select(b => b.ToString("x2")));
        }

        public static void SendEmail(Configuration _config, string email ,string message, string subject)
        {
            SmtpClient client = new SmtpClient(_config.SmtpAdress, _config.SmtpPort);
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential(_config.EmailAddress, _config.EmailPassword);
            client.EnableSsl = true;
            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(_config.EmailAddress);
            mailMessage.To.Add(email);
            mailMessage.Body = message;
            mailMessage.Subject = subject;
            mailMessage.IsBodyHtml = true;
            client.Send(mailMessage);
        }

        public static List<DashboardResponseModel> CanAllocateProject(string connectionString, DashboardResponseModel[] projects, int userId)
        {
            List<int> assignedProjects = new List<int>();
            List<DashboardResponseModel> returnValue = new List<DashboardResponseModel>();
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    string sql = "SELECT DISTINCT ProjectId FROM [dbo].Project WHERE ProjectManagerId = " + userId;
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        SqlDataReader dataReader = cmd.ExecuteReader();

                        while (dataReader.Read())
                        {
                            assignedProjects.Add((int)dataReader["ProjectId"]);
                        }
                    }

                }
            }
            catch (Exception e)
            {

            }

            foreach (var item in projects)
            {
                if (assignedProjects.Exists((assignedProjectId) => assignedProjectId == item.ProjectId))
                {
                    returnValue.Add(item);
                }
            }

            return returnValue;
        }

        public static EmployeeModel GetIdentityUser(ClaimsIdentity httpContextIdentity)
        {
            var identity = httpContextIdentity;
            var deseralizedObject = JsonConvert.DeserializeObject<EmployeeModel>(identity.FindFirst(ClaimTypes.UserData).Value);

            return deseralizedObject;
        }

        public static string RemoveDiacritism(string Text)
        {
            string stringFormD = Text.Normalize(System.Text.NormalizationForm.FormD);
            System.Text.StringBuilder retVal = new System.Text.StringBuilder();
            for (int index = 0; index < stringFormD.Length; index++)
            {
                if (System.Globalization.CharUnicodeInfo.GetUnicodeCategory(stringFormD[index]) != System.Globalization.UnicodeCategory.NonSpacingMark)
                    retVal.Append(stringFormD[index]);
            }
            return retVal.ToString().Normalize(System.Text.NormalizationForm.FormC);
        }

        public static string RandomPassword()
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < 15; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }

            return builder.ToString();
        }


    }
}
