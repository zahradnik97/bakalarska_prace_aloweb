﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace zaht02_BP.Helpers
{
    public class Configuration
    {
        public string ConnectionString { get; set; }
        public string EmailPassword { get; set; }
        public string EmailAddress { get; set; }
        public string Secret { get; set; }
        public string Url { get; set; }
        public int SmtpPort { get; set; }
        public string SmtpAdress { get; set; }
        public int HolidayId { get; set; }
    }
}
