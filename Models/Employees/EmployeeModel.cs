﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using zaht02_BP.Models.Abilities;

namespace zaht02_BP.Models.Employees
{
    public class EmployeeModel
    {
        public int EmployeeId { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int ContractType { get; set; }
        public string Position { get; set; }
        public string[] Roles { get; set; }
        public string Email { get; set; }
        public AbilityModel[] Abilities { get; set; }

        public string Name()
        {
            return String.Format("{0} {1}",FirstName,LastName);
        }

        public string GetLogInfo(string message = "")
        {
            return String.Format("{0} USER: {1} EmployeeId: {2}",message,Name(),EmployeeId);
        }
    }
}
