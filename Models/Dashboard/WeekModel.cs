﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using zaht02_BP.Helpers;
using zaht02_BP.Models.Employees;
using zaht02_BP.Models.Projects;

namespace zaht02_BP.Models.Dashboard
{
    public class WeekModel
    {
        public int WeekId { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public List<DayModel> Days { get; set; }

        public WeekModel(int weekId, string[] days, int employeeId, string connectionString)
        {
            this.WeekId = weekId;
            SetDates(connectionString);
            this.Days = SetDays(days, employeeId, connectionString);
        }

        private void SetDates(string connectionString)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                string sql = "SELECT StartDate,EndDate FROM [dbo].[Week] WHERE WeekId = @WeekId";
                using (SqlCommand cmd = new SqlCommand(sql, connection))
                {
                    cmd.Parameters.Add("@WeekId", SqlDbType.Int).Value = WeekId;
                    SqlDataReader dataReader = cmd.ExecuteReader();

                    while (dataReader.Read())
                    {
                        this.StartDate = dataReader["StartDate"].ToString();
                        this.EndDate = dataReader["EndDate"].ToString();
                    }

                }
            }
        }

        private List<DayModel> SetDays(string[] days, int employeeId, string connectionString)
        {
            var daysList = new List<DayModel>();
            days.ToList().ForEach((item) =>
            {
                var dayId = int.Parse(item);
                DayModel newDay = new DayModel();
                newDay.DayId = dayId;

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    string sql = "SELECT * FROM [dbo].[DayProjectEmployee] " +
                        "INNER JOIN [dbo].Project ON DayProjectEmployee.ProjectId = Project.ProjectId " +
                        "INNER JOIN [dbo].Employee ON Project.ProjectManagerId = Employee.EmployeeId " +
                        "WHERE DayProjectEmployee.EmployeeId = @EmployeeId AND DayId = @DayId";
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.Add("@EmployeeId", SqlDbType.Int).Value = employeeId;
                        cmd.Parameters.Add("@DayId", SqlDbType.Int).Value = dayId;

                        SqlDataReader dataReader = cmd.ExecuteReader();
                        while (dataReader.Read())
                        {
                            newDay.Projects.Add(
                                new ProjectModel()
                                {
                                    ProjectId = (int)dataReader["ProjectId"],
                                    Allocation = (int)dataReader["Allocation"],
                                    ConfirmedRequest = (bool)dataReader["ConfirmedRequest"],
                                    Name = dataReader["Name"].ToString(),
                                    Color = dataReader["Color"].ToString().Trim(),
                                    Client = dataReader["Client"].ToString(),
                                    IsBillable = (bool)dataReader["IsBillable"],
                                    Note = dataReader["Note"].ToString(),
                                    ProjectManager = new EmployeeModel()
                                    {
                                        EmployeeId = (int)dataReader["ProjectManagerId"],
                                        FirstName = dataReader["FirstName"].ToString(),
                                        LastName = dataReader["LastName"].ToString(),
                                        Position = dataReader["Position"].ToString(),
                                        ContractType = (int)dataReader["ContractType"]
                                    }
                                }
                              );
                        }
                    }
                }
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    string sql = "SELECT * FROM [dbo].Day WHERE DayId = @DayId";
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.Add("@DayId", SqlDbType.Int).Value = dayId;
                        SqlDataReader dataReader = cmd.ExecuteReader();
                        while (dataReader.Read())
                        {
                            newDay.Date = dataReader["Date"].ToString();
                            newDay.IsHoliday = (bool)dataReader["IsHoliday"];
                        }
                    }
                }
                daysList.Add(newDay);
            });

            return daysList;
        }
    }
}
