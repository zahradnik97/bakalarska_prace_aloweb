﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using zaht02_BP.Models.Employees;

namespace zaht02_BP.Models.Dashboard
{
    public class DashboardModel
    {
        public EmployeeModel Employee { get; set; }
        public WeekModel Week { get; set; }

    }

    public class DashboardResponseModel
    {
        public int DayProjectEmployeeId { get; set; }
        public int DayId { get; set; }
        public int ProjectId { get; set; }
        public int EmployeeId { get; set; }
        public int Allocation { get; set; }
        public bool ConfirmedRequest { get; set; }

    }
}
