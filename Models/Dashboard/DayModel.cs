﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using zaht02_BP.Models.Projects;

namespace zaht02_BP.Models.Dashboard
{
    public class DayModel
    {
        public int DayId { get; set; }
        public string Date { get; set; }
        public bool IsHoliday { get; set; }
        public List<ProjectModel> Projects { get; set; }

        public DayModel()
        {
            this.Projects = new List<ProjectModel>();
        }
    }
}
