﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace zaht02_BP.Models.CodeTables
{
    public class CodeTableModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
