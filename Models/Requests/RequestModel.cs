﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace zaht02_BP.Models.Requests
{
    public class RequestModel
    {
        public int RequestId { get; set; }
        public string DateTo { get; set; }
        public string DateFrom { get; set; }
        public string Reason { get; set; }
        public int ProjectManagerId { get; set; }
        public int EmployeeId { get; set; }
        public string Type { get; set; }
        public bool IsApprovedByPM { get; set; }
        public bool IsApprovedByHRM { get; set; }
        public bool IsConfirmed { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string GetRequestTypeEmail()
        {
            switch (Type)
            {
                case "HomeOffice":
                    return "Vzdálená práce";
                case "Vacation":
                    return "Dovolená";
                case "SubstituteVacation":
                    return "Náhradní volno";
                case "NoPayVacation":
                    return "Neplacené volno";
                case "BussinessTrip":
                    return "Pracovní cesta";
                default:
                    return null;
            }
        }

    }

    public enum RequestType
    {
        HomeOffice = 1,
        Vacation = 2,
        NoPayVacation = 3,
        BussinessTrip = 4,
        SubstituteVacation = 5
    }

}
