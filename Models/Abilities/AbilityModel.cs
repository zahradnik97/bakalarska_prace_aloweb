﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace zaht02_BP.Models.Abilities
{
    public class AbilityModel
    {
        public int AbilityId { get; set; }
        public string Name { get; set; }
    }
}
