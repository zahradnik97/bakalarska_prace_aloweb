﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using zaht02_BP.Models.Employees;

namespace zaht02_BP.Models.Projects
{
    public class ProjectModel
    {
        public int ProjectId { get; set; }
        public string Name { get; set; }
        public int ProjectManagerId { get; set; }
        public EmployeeModel ProjectManager { get; set; }
        public bool IsBillable { get; set; }
        public string Color { get; set; }
        public int Allocation { get; set; }
        public string Note { get; set; }
        public string Client { get; set; }
        public bool ConfirmedRequest { get; set; }
    }

    public class ProjectModelAll
    {
        public int ProjectId { get; set; }
        public string Name { get; set; }
        public int ProjectManagerId { get; set; }
    }

}
